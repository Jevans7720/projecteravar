import { Component, OnInit } from '@angular/core';

import * as appInfo from '../../app-info';

@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit {

  appInfo;

  constructor() { }

  ngOnInit(): void {
    this.appInfo = appInfo;
  }

}
