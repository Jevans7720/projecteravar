/*
Project lead: Jeff evans
Last edit by Jeff Evans
date: 07/15/2016
*/

//This file contains UI fucntions

function parseMain(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('parseMain');
		else _STACKTRACE_ = new stackTrace('parseMain');
	}
	else _STACKTRACE_ = new stackTrace('parseMain');
	var localErrorCode = -1;


	breakout:{
		//create an empty kingdom object to get a list of properties
		var emptyObj = new kingdomMain(undefined, _STACKTRACE_);
		
		//check for errors creating empty object
		if(emptyObj.lastError >100){localErrorCode = 901; break breakout;}
		
		for(var row in emptyObj){
			
			//Skip functions
			if(typeof emptyObj[row] == 'function')continue;
			_STACKTRACE_.addToStack(row);
			
			//if(emptyObj.lastError >100){localErrorCode = 901; break breakout;}
			//Check against list of properties to ignore
			if(_IGNORE_LIST_KINGDOMMAIN_.indexOf(row) == -1){
				
				//Get first letter and convert to upper case
				var rowName = row[0].toUpperCase()
				
				//Remove lowercase first letter and add upper case.
				rowName = rowName.concat(row.slice(1));
				
				//Add space to ruler spouse 
				if(row == 'rulerSpouse')rowName = rowName.replace("Spouse", " Spouse");
				
				//Build new row and add it to the end
				templateTableMain += '<tr class="tableHeaderSide" proprty="' + row + '"><th>' + rowName + '</th><td>None</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr>';
			}
			_STACKTRACE_.removeFromStack(row);
		}
		//Add rows to main table
		$('#mainTable').html(templateTableMain);
	}
	
 	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('parseMain');
	return localErrorCode;
}

function updateUI(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('updateUI');
		else _STACKTRACE_ = new stackTrace('updateUI');
	}
	else _STACKTRACE_ = new stackTrace('updateUI');
	var localErrorCode = -1;
	
	$('#districtCitySelect').html('<option value="null" disabled selected>Select a City</option><option value="capital">Capital</option>');
	for(var city in kingdom){
		if(city == 'main' || city == 'temp' || city == 'capital') continue;
		$('#districtCitySelect').html($('#districtCitySelect').html() + '<option value="' + city + '">' + city + '</option>');
		$('#districtCitySelect').selectmenu('refresh');
	}
	
	$('#districtCitySelect').html('<option value="null" disabled selected>Select a City</option><option value="capital">Capital</option>');
		for(var city in kingdom){
			if(city == 'main' || city == 'temp' || city == 'capital') continue;
			
			$('#districtCitySelect').html($('#districtCitySelect').html() + '<option value="' + city + '">' + city + '</option>');
			$('#districtCitySelect').selectmenu('refresh');
		}
	
	updateMainTable(_STACKTRACE_);
	
	for(var cities in kingdom){
		if(cities == 'temp' || cities == 'main')continue;
		_STACKTRACE_.addToStack(cities);
		
		//If a tab does not exist for a given city create one
		if($('#' + cities + 'Table').length === 0)buildNewTab(cities, _STACKTRACE_);
		
		//update the tab corrilating to the city being totalled
		updateCityTable(cities, _STACKTRACE_);
		_STACKTRACE_.removeFromStack(cities);
	}

	//Update the dashboard at the top of the page
	updateDash(_STACKTRACE_);
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('updateUI');
	return localErrorCode;
}

function updateMainTable(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('updateMaintable');
		else _STACKTRACE_ = new stackTrace('updateMaintable');
	}
	else _STACKTRACE_ = new stackTrace('updateMaintable');
	var localErrorCode = -1;
	
	var child;
	
	breakout:{
		for(var property in kingdom.main){
			if(typeof kingdom.main[property] === undefined ){localErrorCode = 106; break breakout;}
			if(typeof kingdom.main[property] == 'function')continue;
			if(_IGNORE_LIST_KINGDOMMAIN_.indexOf(property) != -1)continue;
			child = 2;
			_STACKTRACE_.addToStack(property);
			for(var value in kingdom.main[property]){
				if(typeof kingdom.main[property][value] === undefined ){localErrorCode = 106; break breakout;}
				if($("[property=" + property + "]").length === 0){localErrorCode = 701; break breakout;}
				if($("[property=" + property + "] :nth-child(" + child + ")").length === 0){localErrorCode = 702; break breakout;}
				$("[property=" + property + "] :nth-child(" + child + ")").html(kingdom.main[property][value]);
				child ++;
			}
			_STACKTRACE_.removeFromStack(property);
		}
		
		for(var property in kingdom){
			if(property == 'temp')continue;
			if(typeof kingdom[property] === undefined ){localErrorCode = 106; break breakout;}
			if(kingdom[property].constructor != kingdomCity)continue;
			if($('[city=' + property + ']').length === 0)buildMainRow(property,  _STACKTRACE_);
			console.log($('[city=' + property + ']'));
			console.log($('[city=' + property + ']')[0]);
			child = 3;
			for(var value in kingdom[property]){
				if(value != 'E' && value != 'S' && value != 'L')continue;
				if(typeof kingdom[property][value] === undefined ){localErrorCode = 106; break breakout;}
				console.log($('[city=' + property + ']'));
				if($("#mainTable [city=" + property + "]").length === 0){localErrorCode = 701; break breakout;}
				if($("#mainTable [city=" + property + "] :nth-child(" + child + ")").length === 0){localErrorCode = 702; break breakout;}
				$("#mainTable [city=" + property + "] :nth-child(" + child + ")").html(kingdom[property][value]);
				child ++;
			}
		}
		
		child = 3;
		for(var property in kingdom.main){
			if(property != 'E' && property != 'S' && property != 'L')continue;
			if(typeof kingdom.main[property] === undefined ){localErrorCode = 106; break breakout;}
			if($("#main1 .tableHeaderBottom").length === 0){localErrorCode = 701; break breakout;}
			if($("#main1 .tableHeaderBottom :nth-child(" + child + ")").length === 0){localErrorCode = 702; break breakout;}
			$("#main1 .tableHeaderBottom :nth-child(" + child + ")").html(kingdom.main[property]);
			child ++;
		}
	}
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('updateMaintable');
	return localErrorCode;
}

function updateCityTable(city, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('updateCityTable');
		else _STACKTRACE_ = new stackTrace('updateCityTable');
	}
	else _STACKTRACE_ = new stackTrace('updateCityTable');
	var localErrorCode = -1;
	var link = '';
	
	//Clear the table we are working with
	$('#' + city + 'Table tbody').html('');
	
	
	breakout:{
		for(var id in kingdom[city].districts){
			
			//Skip District ID o as it is used to verify data intergrety.
			if(id == 0)continue;
			_STACKTRACE_.addToStack(id);
			
			//If the district object is undefined throw an error
			if(typeof kingdom[city].districts[id] === undefined ){localErrorCode = 107; break;}
			
			//Build a row with a district window button for each district
			buildCityDistRow(city + 'Table', city, id, _STACKTRACE_);
			for(var square in kingdom[city].districts[id].buildings){
				_STACKTRACE_.addToStack(square);
				link = '';
				//Link needs to be changed to an array instead due to issues withthe order blocks are processed in and potential building orientations
				
				//If the square object is undefined throw an error
				if(typeof kingdom[city].districts[id].buildings[square] === undefined ){localErrorCode = 108; _STACKTRACE_.removeFromStack(id); break;}
				for(var block in kingdom[city].districts[id].buildings[square]){
					
					//If the block beign checked is equal to link skip it so that buildings are not counted twice
					if(block == link)continue;
					
					//Skip any blocks marked as empty
					if(kingdom[city].districts[id].buildings[square][block] == 'Empty')continue;
					_STACKTRACE_.addToStack(block);
					
					//If the building object is undefined throw an error
					if(typeof kingdom[city].districts[id].buildings[square][block] === undefined ){localErrorCode = 103; _STACKTRACE_.removeFromStack(square); break;}
					
					//Assign building container to object to cut down on typing
					var buildingContainer = kingdom[city].districts[id].buildings[square][block];
					
					//Build row fo building
					buildCityRow(city + 'Table', buildingContainer.name, id, square, block, _STACKTRACE_);
					
					//If building is size 2 set link to the block the other portion of the building is in
					if(buildingContainer.size == 2)link = buildingContainer.link;
					
					for(var property in buildingContainer){
						_STACKTRACE_.addToStack(property);
						
						
						//Enter the values for the building into the Table
						switch(property){
							case 'E':
								$("#" + city + " [location=" + id + square + block + "] :nth-child(2)").html(buildingContainer[property]);
								break;
							case 'L':
								$("#" + city + " [location=" + id + square + block + "] :nth-child(3)").html(buildingContainer[property]);
								break;
							case 'S':
								$("#" + city + " [location=" + id + square + block + "] :nth-child(4)").html(buildingContainer[property]);
								break;
							case 'value':
								$("#" + city + " [location=" + id + square + block + "] :nth-child(6)").html(buildingContainer[property]);
								break;
							case 'defenceMod':
								$("#" + city + " [location=" + id + square + block + "] :nth-child(5)").html(buildingContainer[property]);
								break;
							case 'minor':
								$("#" + city + " [location=" + id + square + block + "] :nth-child(7)").html(buildingContainer[property]);
								break;
							case 'medium':
								$("#" + city + " [location=" + id + square + block + "] :nth-child(8)").html(buildingContainer[property]);
								break;
							case 'major':
								$("#" + city + " [location=" + id + square + block + "] :nth-child(9)").html(buildingContainer[property]);
								break;
						}
						_STACKTRACE_.removeFromStack(property);
					}
					if(buildingContainer.damaged === true)$("#" + city + " [location=" + id + square + block + "]").addClass('damaged');
					_STACKTRACE_.removeFromStack(block);
					if(buildingContainer.size == 4)break;
				}
				_STACKTRACE_.removeFromStack(square);
			}
			_STACKTRACE_.removeFromStack(id);
		}
		
		for(var property in kingdom[city]){
			_STACKTRACE_.addToStack(property);
			//Add totals for the city tabel to the tabel footer.
			switch(property){
				case 'E':
					$("#" + city + " .tableHeaderBottom  :nth-child(2)").html(kingdom[city][property]);
					break;
				case 'L':
					$("#" + city + " .tableHeaderBottom  :nth-child(3)").html(kingdom[city][property]);
					break;
				case 'S':
					$("#" + city + " .tableHeaderBottom  :nth-child(4)").html(kingdom[city][property]);
					break;
				case 'value':
					$("#" + city + " .tableHeaderBottom  :nth-child(6)").html(kingdom[city][property]);
					break;
				case 'defenceMod':
					$("#" + city + " .tableHeaderBottom  :nth-child(5)").html(kingdom[city][property]);
					break;
				case 'magicItems':
					$("#" + city + " .tableHeaderBottom  :nth-child(7)").html(kingdom[city][property].minorLimit);
					$("#" + city + " .tableHeaderBottom  :nth-child(8)").html(kingdom[city][property].mediumLimit);
					$("#" + city + " .tableHeaderBottom  :nth-child(9)").html(kingdom[city][property].majorLimit);
					break;
			}
			_STACKTRACE_.removeFromStack(property);
		}
	}
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('updateCityTable');
	return localErrorCode;
}

function refreshDistrictWindow(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('refreshDistrictWindow');
		else _STACKTRACE_ = new stackTrace('refreshDistrictWindow');
	}
	else _STACKTRACE_ = new stackTrace('refreshDistrictWindow');
	var localErrorCode = -1;
	
	var city = $('#districtDisplay').attr('city');
	var id = $('#districtDisplay').attr('district');
	populateDistWindow(city, id, _STACKTRACE_);
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('refreshDistrictWindow');
	return localErrorCode;
}

function populateDistWindow(city, districtID, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('fillWindow');
		else _STACKTRACE_ = new stackTrace('fillWindow');
	}
	else _STACKTRACE_ = new stackTrace('fillWindow');
	var localErrorCode = -1;
	
	$('#districtDisplay').attr('city', city);
	$('#districtDisplay').attr('district', districtID);
	$('#districtDisplay section').removeClass('damaged');
	
	breakout:{
		//If buildings object is not defined throw error
		if(kingdom[city].districts[districtID].buildings === undefined){localErrorCode = 103; break breakout;}
		
		//Point district to buildings object to sorten variable typing
		var district = kingdom[city].districts[districtID].buildings;
		for(var square in district){
			_STACKTRACE_.addToStack(square);
			
			//If a square is undefined throw error
			if(district[square] === undefined){localErrorCode = 103; _STACKTRACE_.removeFromStack(square); break;}
			for(var block in district[square]){
				_STACKTRACE_.addToStack(block);
				
				//If a block is undefined throw an error
				if(district[square][block] === undefined){localErrorCode = 103; _STACKTRACE_.removeFromStack(square); break;}
				
				//If buildings is empty add empty to the district window
				if(district[square][block] == "Empty")$('#' + square + ' .' + block).html(district[square][block]);
				else {
					$('#' + square + ' .' + block).html(district[square][block].name);
					if(district[square][block].damaged == true)$('#' + square + ' .' + block).addClass('damaged');
				}
				_STACKTRACE_.removeFromStack(block);
			}	
			_STACKTRACE_.removeFromStack(square);
		}
		
		$('#districtDisplaying').html('City: ' + city[0].toUpperCase().concat(city.slice(1)) + '</br></br>DistrictID: ' + districtID);
	}
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('fillWindow');
	return localErrorCode;
}

function updateDash(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('updateDash');
		else _STACKTRACE_ = new stackTrace('updateDash');
	}
	else _STACKTRACE_ = new stackTrace('updateDash');
	var localErrorCode = -1;
	var value = 0;

	breakout:{
		for(var property in kingdom.main){
			_STACKTRACE_.addToStack(property);
			
			//Check list of dashboard items to update
			if(_DASHBOARD_LIST_.indexOf(property) != -1){
					value = kingdom.main[property];
				
				//Make sure kingdomName is a string, throw error if not
				if(property == 'kingdomName'){
					if(typeof value != 'string'){
						_STACKTRACE_.removeFromStack(property);
						localErrorCode = 194;
						break;
					}
				}
				//Make sure all other parameters are numbers, throw error if not
				else{
					if(typeof value != 'number' || isNaN(value)){
						_STACKTRACE_.removeFromStack(property);
						localErrorCode = 193;
						break;
					}
				}
				//KingomName is stored in an editable field and so is added to the HTML differently
				if(property == 'kingdomName')$('#' + property).val(value);
				else $('#' + property).html(value);
			}
			else{
					_STACKTRACE_.removeFromStack(property);
					continue;
			}
			_STACKTRACE_.removeFromStack(property);
		}
	}
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, []);
	_STACKTRACE_.removeFromStack('updateDash');
	return localErrorCode;
}

function showOverlay(overlayToShow, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('showOverlay');
		else _STACKTRACE_ = new stackTrace('showOverlay');
	}
	else _STACKTRACE_ = new stackTrace('showOverlay');
	var localErrorCode = -1;
	
	$('#overlayContainer *').removeClass('visible');
	
	$('#overlayContainer').addClass('visible');
	$('#' + overlayToShow).addClass('visible');
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('showOverlay');
	return localErrorCode;
}

function hideOverlay(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('hideOverlay');
		else _STACKTRACE_ = new stackTrace('hideOverlay');
	}
	else _STACKTRACE_ = new stackTrace('hideOverlay');
	var localErrorCode = -1;
	
	$('#overlayContainer *').removeClass('visible');
	
	$('#overlayContainer').removeClass('visible');
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('hideOverlay');
	return localErrorCode;
}

function prettifyDistrictGrid(squareToConvert, blockToConvert, menu, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('prettifyDistrictGrid');
		else _STACKTRACE_ = new stackTrace('prettifyDistrictGrid');
	}
	else _STACKTRACE_ = new stackTrace('prettifyDistrictGrid');
	var localErrorCode = -1;
	var prettifiedSquare = '';
	var prettifiedBlock = '';
	var outputString = '';

// 	breakout:{
// 		if(typeof squareToConvert != 'string'){localErrorCode = -10; break breakout;}
// 		if(typeof blockToConvert != 'string'){localErrorCode = -10; break breakout;}
// 		if(typeof menu != 'string'){localErrorCode = -10; break breakout;}
// 		if(menu != 'Option' && menu != 'Build'){localErrorCode = -10; break breakout;}

// 		if(squareToConvert.search('block') != -1){
// 			if(squareToConvert.indexOf('k1') != -1)prettifiedSquare += 'Top ';
// 			else if(squareToConvert.indexOf('k2') != -1)prettifiedSquare += 'Middle ';
// 			else if(squareToConvert.indexOf('k3') != -1)prettifiedSquare += 'Bottom ';
// 			else {localErrorCode = -10; break breakout;}

// 			if(squareToConvert.indexOf('_1') != -1)prettifiedSquare += 'Left ';
// 			else if(squareToConvert.indexOf('_2') != -1)prettifiedSquare += 'Center ';
// 			else if(squareToConvert.indexOf('_3') != -1)prettifiedSquare += 'Right ';
// 			else {localErrorCode = -10; break breakout;}

// 			prettifiedSquare += 'Square';
// 		}
// 		else {localErrorCode = -10; break breakout;}

// 		if(blockToConvert.search('sub') != -1){
// 			if(blockToConvert.indexOf('T') != -1)prettifiedBlock += 'Top ';
// 			else if(blockToConvert.indexOf('B') != -1)prettifiedBlock += 'Bottom ';
// 			else {localErrorCode = -10; break breakout;}

// 			if(blockToConvert.indexOf('L') != -1)prettifiedBlock += 'Left ';
// 			else if(blockToConvert.indexOf('R') != -1)prettifiedBlock += 'Right ';
// 			else {localErrorCode = -10; break breakout;}

// 			prettifiedBlock += 'Block';
// 		}
// 		else {localErrorCode = -10; break breakout;}


// 	}
	//This code is reserved for after the change of block to square and subblock to block.
	breakout:{
		if(typeof squareToConvert != 'string'){localErrorCode = -10; break breakout;}
		if(typeof blockToConvert != 'string'){localErrorCode = -10; break breakout;}
		if(typeof menu != 'string'){localErrorCode = -10; break breakout;}
		if(menu != 'Option' && menu != 'Build'){localErrorCode = -10; break breakout;}

		if(squareToConvert.search('square') != -1){
			if(squareToConvert.indexOf('1_') != -1)prettifiedSquare += 'Top ';
			else if(squareToConvert.indexOf('2_') != -1)prettifiedSquare += 'Middle ';
			else if(squareToConvert.indexOf('3_') != -1)prettifiedSquare += 'Bottom ';
			else {localErrorCode = -10; break breakout;}

			if(squareToConvert.indexOf('_1') != -1)prettifiedSquare += 'Left ';
			else if(squareToConvert.indexOf('_2') != -1)prettifiedSquare += 'Center ';
			else if(squareToConvert.indexOf('_3') != -1)prettifiedSquare += 'Right ';
			else {localErrorCode = -10; break breakout;}

			prettifiedSquare += 'Square';
		}
		else {localErrorCode = -10; break breakout;}

		if(blockToConvert.search('block') != -1){
			if(blockToConvert.indexOf('T') != -1)prettifiedBlock += 'Top ';
			else if(blockToConvert.indexOf('B') != -1)prettifiedBlock += 'Bottom ';
			else {localErrorCode = -10; break breakout;}

			if(blockToConvert.indexOf('L') != -1)prettifiedBlock += 'Left ';
			else if(blockToConvert.indexOf('R') != -1)prettifiedBlock += 'Right ';
			else {localErrorCode = -10; break breakout;}

			prettifiedBlock += 'Block';
		}
		else {localErrorCode = -10; break breakout;}


	}
	outputString = 'Selected square:' + prettifiedSquare + '</br>Selected block:' + prettifiedBlock;

	console.log(prettifiedSquare);
	console.log(prettifiedBlock);
	console.log(outputString);

	$('#district' + menu + 'MenuSelected').html(outputString);
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [squareToConvert, blockToConvert], false);
	_STACKTRACE_.removeFromStack('prettifyDistrictGrid');
	return localErrorCode;
}