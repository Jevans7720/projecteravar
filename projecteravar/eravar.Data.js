/*
Project lead: Jeff evans
Last edit by Jeff Evans
date: 07/15/2016
*/


//Basic JS object to store the data for the app
//This object contains all the main data
function kingdomMain(jsonData, _STACKTRACE_) {
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('kingdomMain constructor');
		else _STACKTRACE_ = new stackTrace('kingdomMain constructor');
	} else _STACKTRACE_ = new stackTrace('kingdomMain constructor');

	var lastErrorCode = -1;
	//Properties to hold details about the kingdom
	{ //Braces added for IDE collapsing
		this.kingdomName = undefined;
		this.description = undefined;
		this.buildPoints = undefined;
		this.spareGold = undefined;
		this.alignment = undefined;
		this.controlDC = undefined;
		this.magicItems = undefined;
		this.E = undefined
		this.S = undefined
		this.L = undefined
		this.unrest = undefined
	}


	//Properties to hold data about kingdom land
	{ //Braces added for IDE collapsing
		this.size = undefined;
		this.farms = undefined;
		this.mines = undefined;
		this.cities = undefined;
		this.roads = undefined;
	}

	//Properties to define the rulers and related bonuses
	{ //Braces added for IDE collapsing
		this.ruler = undefined;
		this.rulerSpouse = undefined;
		this.councilor = undefined;
		this.general = undefined;
		this.diplomat = undefined;
		this.priest = undefined;
		this.magister = undefined;
		this.marshal = undefined;
		this.assassin = undefined;
		this.spymaster = undefined;
		this.treasurer = undefined;
		this.warden = undefined;
	}

	//Properties to define edicts and related bonuses
	{ //Braces added for IDE collapsing
		this.promotion = undefined;
		this.taxation = undefined;
		this.festivals = undefined;
	}

	this.roleList = function() {return ['ruler','rulerSpouse','councilor','general','diplomat','priest','magister','marshal','assassin','spymaster','treasurer','warden',];}
	//A small function to print the last error code generated.
	this.lastError = function() {return lastErrorCode;};
	//Check all input to make sure is conforms to the data scheme
	function inputCheck(property, data, _STACKTRACE_) {
		if (typeof _STACKTRACE_ == 'object') {
			if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('inputCheck');
			else _STACKTRACE_ = new stackTrace('inputCheck');
		} else _STACKTRACE_ = new stackTrace('inputCheck');
		var localErrorCode = -1;
		switch (property) {
			//These cases handle descriptive properties
			case 'description':
			case 'kingdomName':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				//If it matches the format return code 0.
				if (typeof data == 'string') localErrorCode = 0;
				//If the data is null or undefined return 102 to indicated data is missing
				else if (typeof data == null || typeof data === undefined) localErrorCode = 102;
				//Otherwise return 101 to indicate that the data format is wrong
				else localErrorCode = 101;
				break;
			//this case is made to handle the magic item entries
			case 'magicItems':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				//If it matches the format return code 0.
				if (data == null || data === undefined) localErrorCode = 102;
				//Also check all sub items for fomat adherence
				for (var arg in data) {
					_STACKTRACE_.addToStack(arg);
					if (data[arg] == null || typeof data[arg] === undefined) localErrorCode = 102;
					_STACKTRACE_.removeFromStack(arg);
				}
				//Check the data type of all the arguments to make sure they conform to the scheme
				for(var magic in data){
					_STACKTRACE_.addToStack(magic);
					if (typeof data.major != 'object'){localErrorCode = 101;break;}
					_STACKTRACE_.removeFromStack(magic);
				}
				break;
			//These cases are made to handle properties that hold numbers
			case 'unrest':
			case 'controlDC':
			case 'spareGold':
			case 'E':
			case 'S':
			case 'L':
			case 'buildPoints':
			case 'cities':
			case 'size':
			case 'farms':
			case 'roads':
			case 'mines':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				//This is the old format for size, farms, roads, and mines. it is depricated for these properties as of version 1.10
				//If it matches the format return code 0.
				if (typeof data == 'number') localErrorCode = 0;
				else if (typeof data == null  || typeof data === undefined) localErrorCode = 102;
				//This is the new format to take consumtion into account
				else if(typeof data == 'object'){
					for (var args in data) {
						_STACKTRACE_.addToStack(arg);
						if (data[args] == null  || typeof data[arg] === undefined) localErrorCode = 102;
						_STACKTRACE_.removeFromStack(arg);
					}
					//Check the data type of all the arguments to make sure they conform to the scheme
					if (typeof data.name != 'number'){localErrorCode = 101;break;}
					if (typeof data.E != 'number'){localErrorCode = 101;break;}
					if (typeof data.S != 'number'){localErrorCode = 101;break;}
					if (typeof data.L != 'number'){localErrorCode = 101;break;}
					if (typeof data.con != 'number'){localErrorCode = 101;break;}
				}
				else localErrorCode = 101;
				break;
			//the default case is made to handle the variables variables related to leader positions
			default:
				console.log('checking ' + property, data);
				//If the data is null set errorCode 102 to indicated data is missing
				if (data == null  || typeof data === undefined) localErrorCode = 102;
				//If data is not an object exit immediatly and throw an error
				if (typeof data != 'object'){localErrorCode = 101; break;}
				//Do the same for all arguments in the array
				for (var args in data) {
					_STACKTRACE_.addToStack(arg);
					if (data[args] == null  || typeof data[arg] === undefined) localErrorCode = 102;
					_STACKTRACE_.removeFromStack(arg);
				}
				//Check the data type of all the arguments to make sure they conform to the scheme
				if (typeof data.name != 'string'){localErrorCode = 101;break;}
				if (typeof data.E != 'number'){localErrorCode = 101;break;}
				if (typeof data.S != 'number'){localErrorCode = 101;break;}
				if (typeof data.L != 'number'){localErrorCode = 101;break;}
				if (typeof data.con != 'number'){localErrorCode = 101;break;}
		}
		if (localErrorCode == -1) localErrorCode = 0;
		if (localErrorCode > 100) lastErrorCode = localErrorCode;
		if (localErrorCode > 0) printError(localErrorCode, _STACKTRACE_, [property, data]);
		_STACKTRACE_.removeFromStack('inputCheck');
		return localErrorCode;
	}
	
	
	//Define clean method for changing data in properties
	//Name and description are strings all else are arrays
	this.update = function(property, data, _STACKTRACE_) {
		if (typeof _STACKTRACE_ == 'object') {
			if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('kingdomMain.update ' + property);
			else _STACKTRACE_ = new stackTrace('kingdomMain.update ' + property);
		} else _STACKTRACE_ = new stackTrace('kingdomMain.update ' + property);
		//Define the variable that will hold error codes while in the function
		var localErrorCode = -1;
		//Log the property and data we are checking 
		console.log(property, typeof data);
		_STACKTRACE_.addToStack(property);
		switch (property) {
			//These cases handle descriptive properties
			case 'description':
			case 'kingdomName':
			case 'unrest':
			case 'controlDC':
			case 'spareGold':
			case 'E':
			case 'S':
			case 'L':
			case 'buildPoints':
			case 'size':
			case 'farms':
			case 'cities':
			case 'roads':
			case 'mines':
				//All data must be checked against an internal data checking function
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				//If no errors are returned assign the data
				if (localErrorCode === 0) this[property] = data;
				break;
			case 'magicItems':
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				if (localErrorCode === 0) this[property] = data;
				break;
			default:
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				if (localErrorCode === 0) this[property] = {
					"name": data.name,
					"E": data.E,
					"L": data.L,
					"S": data.S
				};
		}
		_STACKTRACE_.removeFromStack(property);
		//If errors were encountered log them with the core error handler
		if ($("#loadingProgress").progressbar('instance') !== undefined) {
			$("#loadingProgress").progressbar("value", ($("#loadingProgress").progressbar("value") + 1))
		}
		if (localErrorCode == -1) localErrorCode = 0;
		if (localErrorCode > 100) lastErrorCode = localErrorCode;
		printError(localErrorCode, _STACKTRACE_, [property, data]);
		_STACKTRACE_.removeFromStack('kingdomMain.update ' + property);
		return localErrorCode;
	}
	//Load data into properties
	for (var property in this) {
		//If the type of the property being checked is a function. If so skip over it
		if (typeof this[property] == 'function') continue;
		_STACKTRACE_.addToStack(property);
		//If the jsonData passed is undefined initialize everything as empty strings
		if (jsonData === undefined) this[property] = "";
		//If only a specific propery is undefined log a 102 error with core error handler
		else if (jsonData[property] === undefined) {
			localErrorCode = 102;
			printError(localErrorCode, _STACKTRACE_, property);
		} 
		//Otherwise call update function to update the data.
		else this.update(property, jsonData[property], _STACKTRACE_);
		_STACKTRACE_.removeFromStack(property);
	}
	if (lastErrorCode == -1) lastErrorCode = 0;
	//console padding for readablity
	console.log('\n==========================================\n');
	_STACKTRACE_.removeFromStack('kingdomMain constructor');
}


function kingdomTemp(_STACKTRACE_) {
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('kingdomTemp constructor');
		else _STACKTRACE_ = new stackTrace('kingdomTemp constructor');
	} else _STACKTRACE_ = new stackTrace('kingdomTemp constructor');
	//This object is to be filled out later
	//At this time nothing in the software will reference this
	console.log('\n==========================================\n');
	_STACKTRACE_.removeFromStack('kingdomTemp constructor');
}

//This object is used as the format for all cities and the capital
function kingdomCity(jsonData, _STACKTRACE_) {
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('KingdomCity constructor');
		else _STACKTRACE_ = new stackTrace('KingdomCity constructor');
	} else _STACKTRACE_ = new stackTrace('KingdomCity constructor');

	//This variable will store error codes to be logged
	var lastErrorCode = -1;
	//set self to refence this so that input check can access this.districts[0]
	var self = this; 
	//if this is not done then when an internal function calls this, it returns the page not kingdomCity
	
	//Special properties for specific uses
	{//Braces added for IDE collapsing
		//This variable is to designate a city as a capital, there can only be one at a time.
		this.isCapital = undefined;
		//City decription properties
		this.cityName = undefined;
		this.description = undefined;
		//This is a special object to store a cities various districts
		this.districts = {};
	}	

	//These are the stats for the city that are calculated for the kingdom
	{//Braces added for IDE collapsing
		this.E = undefined;
		this.S = undefined;
		this.L = undefined;
		this.unrest = undefined;
		this.value = undefined;
		this.defenceMod = undefined;
		this.magicItems = undefined;
	}

	//Short function to return last error code
	this.lastError = function(){return lastErrorCode;};


	function inputCheck(property, data, _STACKTRACE_) {
		if (typeof _STACKTRACE_ == 'object') {
			if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('inputCheck');
			else _STACKTRACE_ = new stackTrace('inputCheck');
		} else _STACKTRACE_ = new stackTrace('inputCheck');
		//Define the variable that will hold error codes while in the function
		var localErrorCode = -1;
		switch (property) {
			//These cases handle descriptive properties
			case 'description':
			case 'cityName':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				//If it matches the format return code 0.
				if (typeof data == 'string') localErrorCode = 0;
				//If the data is null or undefined return 102 to indicated data is missing
				else if (typeof data == null || typeof data === undefined) localErrorCode = 102;
				//Otherwise return 101 to indicate that the data format is wrong
				else localErrorCode = 101;
				break;
			case 'isCapital':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				//If it matches the format return code 0.
				if (typeof data == 'boolean') localErrorCode = 0;
				//If the data is null or undefined return 102 to indicated data is missing
				else if (typeof data == null || typeof data === undefined) localErrorCode = 102;
				//Otherwise return 101 to indicate that the data format is wrong
				else localErrorCode = 101;
				break;
			//this case is made to handle the magic item entries
			case 'magicItems':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				//If it matches the format return code 0.
				if (data == null || data === undefined) localErrorCode = 102;
				//Also check all sub items for fomat adherence
				for (var arg in data) {
					_STACKTRACE_.addToStack(arg);
					if (data[arg] == null || typeof data === undefined) localErrorCode = 102;
					_STACKTRACE_.removeFromStack(arg);
				}
				//Check the data type of all the arguments to make sure they conform to the scheme
				for(var magic in data){
					_STACKTRACE_.addToStack(magic);
					if (typeof data.major != 'object'){localErrorCode = 101;break;}
					_STACKTRACE_.removeFromStack(magic);
				}
				break;
			//These cases are made to handle properties that hold numbers
			case 'unrest':
			case 'value':
			case 'defenceMod':
			case 'E':
			case 'S':
			case 'L':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				//If it matches the format return code 0.
				if (typeof data == 'number') localErrorCode = 0;
				else if (typeof data == null  || typeof data === undefined) localErrorCode = 102;
				else localErrorCode = 101;
				break;
			//This is a very special case for handling district data.
			case 'districts':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				if (typeof data == 'object') {
					//Check districts by creating a new district object in this.districs[0]
					self[property][0] = new cityDistrict(data, self.cityName, _STACKTRACE_);
					//If no errors were encountered the data is valid and can be used 
					localErrorCode = self[property][0].lastError();
				} else {
					if (typeof data == null || typeof data === undefined) localErrorCode = 102;
					localErrorCode = 101;
				}
				break;
		}
		if (localErrorCode == -1) localErrorCode = 0;
		if (localErrorCode > 100) lastErrorCode = localErrorCode;
		printError(localErrorCode, _STACKTRACE_, [property, data]);
		_STACKTRACE_.removeFromStack('inputCheck');
		return localErrorCode;
	}
	//Define clean method for changing data in properties
	//Name and description are strings all else are arrays
	this.update = function(property, data, _STACKTRACE_) {
		if (typeof _STACKTRACE_ == 'object') {
			if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('KingdomCity update ' + property);
			else _STACKTRACE_ = new stackTrace('KingdomCity update ' + property);
		} else _STACKTRACE_ = new stackTrace('KingdomCity update ' + property);
		//Define the variable that will hold error codes while in the function
		var localErrorCode = -1;
		//Log the property and data we are checking 
		console.log(property, typeof data);
		switch (property) {
			//These cases handle descriptive properties
			case 'description':
			case 'cityName':
			case 'isCapital':
			case 'unrest':
			case 'value':
			case 'defenceMod':
			case 'E':
			case 'S':
			case 'L':
				//All data must be checked against an internal data checking function
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				//If no errors are returned assign the data
				if (localErrorCode === 0) this[property] = data;
				break;
			case 'magicItems':
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				if (localErrorCode === 0) this[property] = data;
				break;
				//special case for district object as it is not defined at this time
			case 'districts':
				for(var district in data){
					_STACKTRACE_.addToStack(district);
					//Run an input check for each district
					localErrorCode = inputCheck(property, data[district], _STACKTRACE_);
					if (localErrorCode === 0) {
						//If check is successful set to district object under its id number
						this[property][data[district].districtID] = new cityDistrict(data[district], this.cityName, _STACKTRACE_);
						//Set district[0] to undefined to test the next function and prevent accessing bad data
						this[property][0] = undefined;
					}
					_STACKTRACE_.removeFromStack(district);
				}
				break;
		}
		//If errors were encountered log them with the core error handler
		if (localErrorCode == -1) localErrorCode = 0;
		if (localErrorCode > 100) lastErrorCode = localErrorCode;
		printError(localErrorCode, _STACKTRACE_, [property, data]);
		_STACKTRACE_.removeFromStack('KingdomCity update ' + property);
	}
	//Load data into properties
	for (var property in this) {
		//If the type of the property being checked is a function. If so skip over it
		if (typeof this[property] == 'function') continue;
		_STACKTRACE_.addToStack(property);
		//If the jsonData passed is undefined initialize everything as empty strings
		if (jsonData === undefined) this[property] = "";
		//if only a specific propery is undefined log a 102 error with core error handler
		else if (jsonData[property] === undefined) {
			localErrorCode = 102;
			printError(localErrorCode, _STACKTRACE_, property);
			//break;
		} else this.update(property, jsonData[property], _STACKTRACE_);
		_STACKTRACE_.removeFromStack(property);
	}
	//console padding for readablity
	console.log('\n==========================================\n');
	_STACKTRACE_.removeFromStack('KingdomCity constructor');
}

//This object will be used for city districts
function cityDistrict(jsonData, parent, _STACKTRACE_) {
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('cityDistrict constructor');
		else _STACKTRACE_ = new stackTrace('cityDistrict constructor');
	} 
	else _STACKTRACE_ = new stackTrace('cityDistrict constructor');
	var self = this;
	var lastErrorCode = -1;
	
	var num = 0;
	this.districtID = num;
	
	this.city = parent;

	//define the building grid, and building object
	var blocks = function() {this.blockTL = 'Empty';this.blockTR = 'Empty';this.blockBL = 'Empty';this.blockBR = 'Empty';}
	this.buildings = {
		'square1_1': new blocks(),
		'square1_2': new blocks(),
		'square1_3': new blocks(),
		'square2_1': new blocks(),
		'square2_2': new blocks(),
		'square2_3': new blocks(),
		'square3_1': new blocks(),
		'square3_2': new blocks(),
		'square3_3': new blocks()
	};

	//Object to caontain walls/water borders
	this.walls = {
		'wallNorth': 'none',
		'wallEast': 'none',
		'wallSouth': 'none',
		'wallWest': 'none',
	}

	//This object is used to hold the links between districts.
	this.borders = {
		'borderNorth': {},
		'borderEast': {},
		'borderSouth': {},
		'borderWest': {},
	}

	this.lastError = function() {return lastErrorCode;};

	function inputCheck(property, data, _STACKTRACE_) {
		var localErrorCode = -1;
		_STACKTRACE_.addToStack('inputCheck');
		switch (property) {
			//These cases handle descriptive properties:
			case 'districtID':
				//Log the property and data we are checking 
				console.log('checking ' + property, data);
				//If it matches what we need return code 0.
				if (typeof data == 'number') localErrorCode = 0;
				//If the data is null return 102 to indicated data is missing
				else if (typeof data == null || typeof data == undefined) localErrorCode = 102;
				//Otherwise return 101 to indicate that the data format is wrong
				else localErrorCode = 101;
				break;
				//this case will handle checking the district building layout
			case 'buildings':
				console.log('checking ' + property, data);
				//due to the complex nature of the district layout a special function is used to verify the data.
				if (typeof data == 'object') localErrorCode = checkBuildings(data, _STACKTRACE_); 
				else if (typeof data == null || typeof data == undefined) localErrorCode = 102;
				else localErrorCode = 101;
				break;
			{
				// case 'sinlgeBuilding':
				// 	console.log('checking ' + property, data);
				// 	//due to the complex nature of the district layout a special function is used to verify the data.
				// 	if (typeof data == 'object') {
				// 		localErrorCode = updateBuilding(data, _STACKTRACE_);
				// 	} else {
				// 		if (typeof data[0] == null || typeof data == undefined) localErrorCode = 102;
				// 		localErrorCode = 101;
				// 	}
				// 	break;
			}
			//This case will handle checking for walls
			//may need future modification
			case 'walls':
				console.log('checking ' + property, data);
				//walls must be an object
				if (typeof data == 'object') {
					//for now the data in walls is stored as strings
					for (var side in data) {
						_STACKTRACE_.addToStack(side);
						switch (data[side]) {
							//only 3 values are accepted all others mean errors
							case 'none':
							case 'wall':
							case 'water':
								break;
							default:
								localErrorCode = 101;
						}
						_STACKTRACE_.removeFromStack(side);
					}
					localErrorCode = 0;
				}
				else if (typeof data[0] == null) localErrorCode = 102;
				else localErrorCode = 101;
				break;

		}
		if(localErrorCode > 100) lastErrorCode = localErrorCode;
		if(localErrorCode > 0)printError(localErrorCode, _STACKTRACE_, [property, data]);
		_STACKTRACE_.removeFromStack('inputCheck');
		return localErrorCode;
	}

	//This function is for updating a buildings data
	this.updateBuilding = function (square, block, property, value,  _STACKTRACE_){
		if(typeof _STACKTRACE_ == 'object'){
			if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('updateBuilding');
			else _STACKTRACE_ = new stackTrace('updateBuilding');
		}
		else _STACKTRACE_ = new stackTrace('updateBuilding');
		var localErrorCode = -1;
		breakout: {
			//Check all passed paramerter for valid values
			if(property === undefined){localErrorCode = -10; break breakout;}
			if(value === undefined){localErrorCode = -10; break breakout;}
			switch(square){
				case 'square1_1':
				case 'square1_2':
				case 'square1_3':
				case 'square2_1':
				case 'square2_2':
				case 'square2_3':
				case 'square3_1':
				case 'square3_2':
				case 'square3_3':
					break;
				default:
					localErrorCode = -10;
					break breakout;
			}
			switch(block){
				case 'blockTR':
				case 'blockTL':
				case 'blockBR':
				case 'blockBL':
					break;
				default:
					localErrorCode = -10;
					break breakout;
			}
			//checl to make sure the building data is not corrupt of empty
			if(self.buildings[square][block] == 'Empty'){localErrorCode = 50; break breakout;}
			if(self.buildings === undefined){localErrorCode = 103; break breakout;}
			if(self.buildings[square] === undefined){localErrorCode = 103; break breakout;}
			if(self.buildings[square][block] === undefined){localErrorCode = 103; break breakout;}
			switch(property){
				case 'name':
				case 'nameInternal':
				case 'cost':
				case 'E':
				case 'L':
				case 'S':
				case 'size':
				case 'minor':
				case 'medium':
				case 'major':
				case 'halfs':
				case 'value':
				case 'houseRequired':
				case 'defenceMod':
				case 'unrest':
				case 'limit':
					//all values that cannot be changed return code 52
					localErrorCode = 52;
					break breakout;
				case 'link':
					//buildings of size 1 do not ahve a link value so it cannot be changed
					if(self.buildings[square][block].size == 1){localErrorCode = 52; break breakout;}
					switch(value){
						//check for valid values otherwise throw an error
						case 'blockTR':
						case 'blockTL':
						case 'blockBR':
						case 'blockBL':
							self.buildings[square][block][property] = value;
							localErrorCode = 0;
							break;
						default:
							localErrorCode = -10;
							break breakout;
					}
					break;
				case 'damaged':
					switch(value){
						//check for valid values otherwise throw an error
						case true:
						case false:
							self.buildings[square][block][property] = value;
							localErrorCode = 0;
							break;
						default:
							localErrorCode = -10;
							break breakout;
					}
					break;
				default:
					localErrorCode = -10;
					break breakout;
			}
			
		}
		if(localErrorCode == -1)localErrorCode = 0 
		if(localErrorCode !== 0)printError(localErrorCode, _STACKTRACE_, [square, block, property, value], false);
		_STACKTRACE_.removeFromStack('updateBuilding');
		return localErrorCode;
	}

	//This function is for creating new buildings
	this.newBuilding = function(square, block, building, _STACKTRACE_){
		if(typeof _STACKTRACE_ == 'object'){
			if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('newBuilding');
			else _STACKTRACE_ = new stackTrace('newBuilding');
		}
		else _STACKTRACE_ = new stackTrace('newBuilding');
		var localErrorCode = -1;
		
		breakout: {
			if(this.buildings[square][block] != 'Empty'){localErrorCode = 50; break breakout;}
			if(this.buildings === undefined){localErrorCode = 103; break breakout;}
			if(this.buildings[square] === undefined){localErrorCode = 103; break breakout;}
			if(this.buildings[square][block] === undefined){localErrorCode = 103; break breakout;}
			if(buildings[building] === undefined){localErrorCode = 104; break breakout;}
			this.buildings[square][block] = JSON.parse(JSON.stringify(buildings[building]));
		}

		if(localErrorCode == -1)localErrorCode = 0 
		_STACKTRACE_.removeFromStack('newBuilding');
		return localErrorCode;
	}

	//this function is for checking the integrety of the buildings object.
	function checkBuildings(data, _STACKTRACE_) {
		_STACKTRACE_.addToStack('checkBuildings');
		var localErrorCode = -1;
		for (var square in self.buildings) {
			_STACKTRACE_.addToStack(square);
			switch (typeof data[square]) {
				//Check for empty data
				case 'undefined':
				case 'null':
					localErrorCode = 103;
					break;
				case 'object':
					for (var block in self.buildings[square]) {
						_STACKTRACE_.addToStack(block);
						switch (typeof data[square][block]) {
							//Check for empty data
							case 'undefined':
							case 'null':
								localErrorCode = 103;
								break;
							case 'object':
								_STACKTRACE_.addToStack(data[square][block].name);
								//Set the building we are checking to a variable for easier typing
								var objToCheck = data[square][block];
								//Get expected data of the building we are checking
								var objCheckAgainst = buildings[data[square][block].nameInternal];
								for (var buildingProperty in objCheckAgainst) {
									if (buildingProperty == 'link') continue;
									if (buildingProperty == 'damaged') continue;
									_STACKTRACE_.addToStack(buildingProperty);
									//Check all static values agains the default
									//If any fail through an error
									if (JSON.stringify(objToCheck[buildingProperty]) != JSON.stringify(objCheckAgainst[buildingProperty])) {localErrorCode = 103;}
									_STACKTRACE_.removeFromStack(buildingProperty);
								}
								if (objToCheck.size >= 2) {
									//If the building is medium or larger call the large function to make sure all blocks are linked properly
									localErrorCode = large(data[square], objToCheck, block, _STACKTRACE_);
								}
								_STACKTRACE_.removeFromStack(data[square][block].name);
								break;
							case 'string':
								_STACKTRACE_.addToStack(data[square][block]);
								if (data[square][block] == 'Empty');
								else {
									//If a string other than empty is foudn attempt to convert it to a building object
									localErrorCode = convertString(data[square], block, _STACKTRACE_);
									if(localErrorCode > 100)break;
									//If the building that is made is medium or larger call large to set the link properties
									if (data[square][block].size >= 2) {
										localErrorCode = large(data[square], data[square][block], block, _STACKTRACE_);
									}
								}
								_STACKTRACE_.removeFromStack(data[square][block]);
								break;
							default:
								localErrorCode = 103;
						}
						_STACKTRACE_.removeFromStack(block);
					}
					break;
				default:
					localErrorCode = 103;
			}
			_STACKTRACE_.removeFromStack(square);
		}
		if(localErrorCode == -1)localErrorCode = 0;
		if(localErrorCode > 100) lastErrorCode = localErrorCode;
		_STACKTRACE_.removeFromStack('checkBuildings');
		return localErrorCode;
	}

	//This function only converts internal name strigns to objects for buildings
	function convertString(object, property, _STACKTRACE_) {
		var localErrorCode = -1;
		_STACKTRACE_.addToStack('convertString');
		if (buildings[object[property]] !== undefined) {
			//To prevent assinging of an object by reference the object is turned into a JSON string, then the strign is parsed and assigned.
			var temp = JSON.stringify(buildings[object[property]]);
			object[property] = JSON.parse(temp);
			localErrorCode = 0;
		}
		//If the internal name given is not found throw an error
		else localErrorCode = 104;
		if(localErrorCode == -1)localErrorCode = 0;
		if(localErrorCode > 100) lastErrorCode = localErrorCode;
		_STACKTRACE_.removeFromStack('convertString');
		return localErrorCode;
	}

	//This function is used to link large builings together 
	function large(square, objToCheck, block, _STACKTRACE_) {
		_STACKTRACE_.addToStack('large');
		var localErrorCode = -1;
		var link = objToCheck.link;
		var property;
		var result;
		var invalidLink;
		if (objToCheck.size == 2) {
			switch (block) {
				case 'blockTL':
					invalidLink = 'blockBR';
					break;
				case 'blockTR':
					invalidLink = 'blockBL';
					break;
				case 'blockBL':
					invalidLink = 'blockTR';
					break;
				case 'blockBR':
					invalidLink = 'blockTR';
					break;
				default:
					localErrorCode = 105;
			}
			//If the link is set to a non-adjacent block return error
			if (objToCheck.link == invalidLink) localErrorCode = 105;
			if (typeof square[link] == 'string') {
				//If the block being linked too is a string convert it to a building
				localErrorCode = convertString(square, link, _STACKTRACE_);
				square[link].link = block;
			}
			//Check all properties for mismatched data, if data does not match throw error
			for (property in square[link]) {
				//link will be different so it can be skipped
				if (property == 'link') continue;
				_STACKTRACE_.addToStack(property);
				if (JSON.stringify(square[link][property]) != JSON.stringify(objToCheck[property]))localErrorCode = 70;
				_STACKTRACE_.removeFromStack(property);
			}
			//If the other building does not link back throw error
			console.log(square[link])
			if (square[link].link != block) localErrorCode = 105;
		}
		if (objToCheck.size == 4) {
			//Size 4 buildings only need to make sure they take up a whole square
			for (var property in square) {
				//All properties must match just like size 2
				if (property == block) continue;
				_STACKTRACE_.addToStack(property);
				if (square[property] == null)localErrorCode = 105;
				if (typeof square[property] == 'string') {
					localErrorCode = convertString(square, property, _STACKTRACE_);
					square[property].link = block;
				}
				for (var property2 in square[property]) {
					//For size 4 all blocks that are not the origin block must link to the origin block
					if (property2 == 'link') continue;
					_STACKTRACE_.addToStack(property2);
					if (JSON.stringify(square[property][property2]) != JSON.stringify(objToCheck[property2]))localErrorCode = 70;
					_STACKTRACE_.removeFromStack(property2);
				}
				_STACKTRACE_.removeFromStack(property);
			}
		}
		if(localErrorCode == -1)localErrorCode = 0;
		if(localErrorCode > 100) lastErrorCode = localErrorCode;
		printError(localErrorCode, _STACKTRACE_, [block, objToCheck, link, invalidLink, square[link]], false)
		_STACKTRACE_.removeFromStack('large');
		return localErrorCode;
	}
	this.update = function(property, data, _STACKTRACE_) {
		if (typeof _STACKTRACE_ == 'object') {
			if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('cityDistrict update' + property);
			else _STACKTRACE_ = new stackTrace('cityDistrict update' + property);
		} 
		else _STACKTRACE_ = new stackTrace('cityDistrict update' + property);
		//Define the variable that will hold error codes while in the function
		var localErrorCode = -1
		//Log the property and data we are checking 
		console.log(property, typeof data);
		switch (property) {
			//These cases handle descriptive properties
			case 'districtID':
				//All data must be checked against an internal data checking function
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				//If no errors are returned assign the data
				if (localErrorCode === 0) this[property] = data;
				break;
			case 'walls':
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				if (localErrorCode === 0) this[property] = data;
				break;
			case 'buildings':
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				if (localErrorCode === 0) this[property] = data;
				break;
			case 'singleBuilding':
				localErrorCode = inputCheck(property, data, _STACKTRACE_);
				if (localErrorCode === 0) this[property] = data;
				break;

			case 'borders':
				console.log('Borders cannot be modified at this time');
				localErrorCode = 1;
				break;
		}
		//If errors were encountered log them with the core error handler
		if(localErrorCode > 100) lastErrorCode = localErrorCode;
		if (localErrorCode > 0) printError(localErrorCode, _STACKTRACE_, [property, data]);
		_STACKTRACE_.removeFromStack('cityDistrict update' + property);
	}
	console.log('\n==========================================\n');
	console.log('Loading District');
	console.log('\n==========================================\n');
	for (var property in this) {
		//If the type of the property being checked is a function. If so skip over it
		if (typeof this[property] == 'function') continue;
		if (property == 'city') continue;
		_STACKTRACE_.addToStack(property);
		//If the jsonData passed is undefined initialize everything as empty strings
		if (jsonData === undefined){localErrorCode = 102;_STACKTRACE_.removeFromStack(property); break;}
		if (jsonData == 'new'){
			var newID;
			for(var id in kingdom[parent].districts){
				if(id == 0)num++;
				num++;
			}
			for(var i = 1; i < num; i++){
				_STACKTRACE_.addToStack(i);
				if(kingdom[parent].districts[i] === undefined){
					newID = i;
					_STACKTRACE_.removeFromStack(i);
					break;}
				_STACKTRACE_.removeFromStack(i);
			}
			this.update('districtID', newID, _STACKTRACE_ );
			_STACKTRACE_.removeFromStack(property);
			break;
		}
		//if only a specific propery is undefined log a 102 error with core error handler
		else if (jsonData[property] === undefined) {
			lastErrorCode = 102;
			printError(lastErrorCode, _STACKTRACE_, property);
			//break;
		} else this.update(property, jsonData[property], _STACKTRACE_);
		_STACKTRACE_.removeFromStack(property);
	}
	console.log('\n==========================================\n');
	_STACKTRACE_.removeFromStack('cityDistrict constructor');
	if (lastErrorCode == -1)lastErrorCode = 0;
}