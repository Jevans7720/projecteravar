/*
Project lead: Jeff evans
Last edit by Jeff Evans
date: 07/12/2016
*/

//This file contains all functions and objects needed for debugging. 
function stackTrace(start){
	//Variable to store the stack stace
	var stack = [start];

	this.printStack = function(){
		return stack.join(' -> ');
	}
	this.addToStack = function(path){
		stack.push(path)
		if(SUPER_DEBUG === true){
			console.log(path + ' is entering the stack.');
			console.log(this.printStack());
		}
	}
	this.removeFromStack = function(path){
		path = path || '';
		stack.pop()
		if(SUPER_DEBUG === true){
			console.log(path + ' is leaving the stack.');
			console.log(this.printStack());
		}
	}

	this.resetStack = function(){
		stack = [stack[0]];
	}
}

//Core error handler, error code and caller function are required. Paramters are not but advised.
//This may need to be moved to eravar.Error.js if the function gets too long
function printError(errorNumber, stack, params, alertUser){
	alertUser = alertUser || false;
	params = params || [];
	var message = '';
	console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
	console.log('');
	//Log error number, caller fucntion and any paramaters that were passed
	if (typeof stack == 'object')console.log(errorNumber, stack.printStack(), params);
	//Log error details based on error code
	switch(errorNumber){
		case -99:
			message = ('DEBUG: CUSTOM ERROR CALL FOR DEBUGGING');
			break;
		case -10:
			message = ("ERROR: INVALID PARAMETER PASSED.");
			break;
		case 0:
			message = ("INFO: No error encountered. All is well.");
			break;
/////////////////////////////////////////////////////////////////////////////////////////
		case 1:
			message = ("INFO: This feature has not yeat been implemented. Hang in there folks.");
			break;
		case 2:
			message = ("INFO: Kingdom file loaded successfully.");
			break;
		case 3:
			message = ("INFO: Unable to load kingdom file as it was not found.");
			break;
		case 4:
			message = ("INFO: No kingdom file specified.");
			break;
		case 20:
			message = ("INFO: Found city in kingdom using incorrect data scheme. Could not include in kingodm total.");
			break;
		case 50:
			message = ("INFO: City block already occupied. Could not build building.");
			alertUser = true;
			break;
		case 51:
			message = ("INFO: City block is empty, no building data to update.");
			break;
		case 52:
			message = ("INFO: This value cannot be changed");
			break;
		case 53:
			message = ("INFO: Insufficient build points");
			alertUser = true;
			break;
	 case 70:
			message = ("WARN: DATA MISMATCH IN LARGE BUILDING.");
			alertUser = true;
			break;
	 case 100: 
			message = ("WARN: Errors were encountered while processing your request.");
			break;
/////////////////////////////////////////////////////////////////////////////////////////
	 case 101: 
			message = ("ERROR: FORMATTING ERROR. JSON DATA MAY BE CORRUPT");
			break;
	 case 102:
			message = ("ERROR: DATA ERROR. DATA IS MISSING FROM JSON FILE");
			break;
	 case 103:
			message = ("ERROR: DATA ERROR. BUILDING DATA MISSING OR COPRRUPT");
			break;
	 case 104:
			message = ("ERROR: DATA ERROR. BUIDLING WITH UNKNOWN INTERNAL NAME FOUND");
			break;
	 case 105:
			message = ("ERROR: DATA ERROR. BUILDING OF SIZE 2+ USING INVALID NUMBER OF SPACES");
			break;
	 case 106:
			message = ("ERROR: DATA ERROR. MAIN DATA MISSING OR COPRRUPT");
			break;
	 case 107:
			message = ("ERROR: DATA ERROR. CITY DATA MISSING OR COPRRUPT");
			break;
	 case 108:
			message = ("ERROR: DATA ERROR. DISTRICT DATA MISSING OR COPRRUPT");
			break;
/////////////////////////////////////////////////////////////////////////////////////////
	 case 111:
			message = ("ERROR: CITY DISTRICT DATA MISSING WHEN UPDATING CITY TOTALS. COULD NOT UPDATE CITY");
			break;
	 case 112:
			message = ("ERROR: CITY DISTRICT DATA CORRUPT WHEN UPDATING CITY TOTALS. COULD NOT UPDATE CITY");
			break;
		case 113:
			message = ("ERROR: BUILDING DATA DOES NOT MATCH BUILDING LIST. COULD NOT UPDATE CITY");
			break;
		case 114:
			message = ("ERROR: CAPITAL DATA MISSING FROM KINGDOM. COULD NOT UPDATE KINGDOM");
			break;
		case 115:
			message = ("ERROR: MAIN DATA MISSING FROM KINGDOM. COULD NOT UPDATE KINGDOM");
			break;
		case 191:
			message = ("ERROR: GOLD VALUE CANNOT BE 0 OR LESS WHEN DEPOSITING GOLD");
			break;
		case 192:
			message = ("ERROR: GOLD VALUE MUST BE A NUMBER");
			break;
		case 193:
			message = ("ERROR: VALUE OF KINGDOM PROPERTY IS NOT A NUMBER");
			break;
/////////////////////////////////////////////////////////////////////////////////////////
		case 701:
			message = ("ERROR: TRIED TO ACCESS NON EXISTANT TABLE ROW. COULD NOT UPDATE TABLE.");
			break;
		case 702:
			message = ("ERROR: TRIED TO ACCESS NON EXISTANT TABLE CELL. COULD NOT UPDATE TABLE.");
			break;
/////////////////////////////////////////////////////////////////////////////////////////
		case 901:
			message = ("ERROR: UNABLE TO CREATE EMPTY KINGDOMMAIN OBJECT. COULD NOT BUILD TABLE");
			break;
		default:
			//this error displays if an unknown error code is thrown. If this comes up shit is serious buisiness
			console.log("FATAL ERROR: !!!UNKNOWN ERROR ENCOUNTERED!!!");
			alert("FATAL ERROR: !!!UNKNOWN ERROR ENCOUNTERED!!!\nA fatal error was encountered. The program has been halted.\nClosing this window will cause the page to reload.");
			location.reload();
			throw ('fatal error');
	}
	console.log(message);
	if(errorNumber > 100 || alertUser === true)console.log('Alerting User');
	if(errorNumber > 100 || alertUser === true)alert(message);
	console.log('');
	console.log('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~');
}
