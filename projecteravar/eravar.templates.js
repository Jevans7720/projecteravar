/*
Project lead: Jeff evans
Last edit by Jeff Evans
date: 07/12/2016
*/

//This file stores any long winded/repetative html required for use in the HTML as well as any templates.

var templateTableMain ='';

function buildMainTemplate(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('buildMainTemplate');
		else _STACKTRACE_ = new stackTrace('buildMainTemplate');
	}
	else _STACKTRACE_ = new stackTrace('buildMainTemplate');
	var localErrorCode = -1;

	breakout:{
		//Create empty object to get kingdom properties
		var emptyObj = new kingdomMain(undefined, _STACKTRACE_);
		//Throw errors if the empty object encountered any
		if(emptyObj.lastError >100){localErrorCode = 901; break breakout;}

		for(var row in emptyObj){
			//Skip funtions
			if(typeof emptyObj[row] == 'function')continue;
			_STACKTRACE_.addToStack(row);
			
			//Check ignore list
			if(_IGNORE_LIST_KINGDOMMAIN_.indexOf(row) == -1){
			//Capitalize row na,e
				var rowName = row[0].toUpperCase()
				rowName = rowName.concat(row.slice(1));
				
				//Add a space to rulerSpouse
				if(row == 'rulerSpouse')rowName = rowName.replace("Spouse", " Spouse");
				
				//Append row to main table data
				templateTableMain += '<tr class="tableHeaderSide" property="' + row + '"><th>' + rowName + '</th><td>None</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr>';
			}
			_STACKTRACE_.removeFromStack(row);
		}
		//Send table data to table
		$('#mainTable').html(templateTableMain);
	}
	
 	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('buildMainTemplate');
	return localErrorCode;

}

function buildMainRow(city,  _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('buildMainRow');
		else _STACKTRACE_ = new stackTrace('buildMainRow');
	}
	else _STACKTRACE_ = new stackTrace('buildMainRow');
	var localErrorCode = -1;
	
	//Generate row for main table to contain city totals
	var row = '<tr class="tableHeaderSide" city=' + city + '><th>' + city + '</th><td>None</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr>'

	//Append row to table
	$('#mainTable tbody').html($('#mainTable tbody').html() + row);
	
 	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('buildMainRow');
	return localErrorCode;
}

function buildCityDistRow(table, city, district, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('buildCityDistRow');
		else _STACKTRACE_ = new stackTrace('buildCityDistRow');
	}
	else _STACKTRACE_ = new stackTrace('buildCityDistRow');
	var localErrorCode = -1;
	
	//Generate Button to show city district window for city district
	$('#DOMCREATOR').html("<tr class=\"districtWindowButton tableHeaderSide\" id=\"" + city + "Dist" + district + "\"><th></th><td colspan=\"8\"><button id=\"" + city + "Dist" + district + "Button\">Open District window for " + city + " District " + district + "</button></td></tr>");
	
	//Append row and turn into JqueryUI button
	$('#' + table + ' tbody')[0].appendChild($('#DOMCREATOR :nth-child(1)')[0]);
	$('#DOMCREATOR :nth-child(1)').html('');
	$('#' + city + 'Dist' + district + 'Button').button();
	$('#' + city + 'Dist' + district + 'Button').click(function(){
		populateDistWindow(city,district, _STACKTRACE_);
		$('#tableContainer').removeClass('visible');
		$('#districtContainer').addClass('visible');
	});
	
 	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('buildCityDistRow');
	return localErrorCode;
}

function buildCityRow(table, building, district, square, block, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('buildCityRow');
		else _STACKTRACE_ = new stackTrace('buildCityRow');
	}
	else _STACKTRACE_ = new stackTrace('buildCityRow');
	var localErrorCode = -1;
	
	//Generate new row for city table
	$('#DOMCREATOR').html('<tr class="tableHeaderSide" location=' + district + square + block + '><th>' + building + '</th><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>');

	//Append row to table
	$('#' + table + ' tbody')[0].appendChild($('#DOMCREATOR :nth-child(1)')[0]);
	$('#DOMCREATOR :nth-child(1)').html('');
	
 	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('buildCityRow');
	return localErrorCode;
}

function buildNewTab(tabName, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('buildNewTab');
		else _STACKTRACE_ = new stackTrace('buildNewTab');
	}
	else _STACKTRACE_ = new stackTrace('buildNewTab');
	var localErrorCode = -1;
	
	//Generate Id for new tab
	var newID = 1+$('#tableContainer li').length;
	
	//Generate li html for new tab
	var newTab = '<li aria-expanded="false" aria-selected="false" aria-labelledby="ui-id-' + newID + '" aria-controls="' + tabName + '" tabindex="-1" role="tab" class="ui-state-default ui-corner-top"><a id="ui-id-' + newID + '" tabindex="-1" role="presentation" class="ui-tabs-anchor" href="#' + tabName + '">' + tabName + '</a></li>'
	
	//Generate new table for new tab
	var newTable = '<div aria-hidden="false" style="display: block;" role="tabpanel" class=" cityTable ui-tabs-panel ui-widget-content ui-corner-bottom" aria-labelledby="ui-id-' + newID + '" id="' + tabName + '"><table id="' + tabName + 'TableHead" class="ui-corner-top tableHeader"><tbody><tr class="tableHeaderTop"><th class="cornerStoneTop">Building</th><th>Economy</th><th>Loyalty</th><th>Stability</th><th>Defence Mod</th><th>Value</th><th>Minor</th><th>Medium</th><th>Major</th><th class="scrollPadding"></th></tr></tbody></table><div id="scrollTable"><table id="' + tabName + 'Table"><tbody></tbody></table></div><table id="capitalTableFoot" class="ui-corner-bottom tableFooter"><tbody><tr class="tableHeaderBottom"><th class="cornerStoneBottom">Totals</th><th>N/A</th><th>0</th><th>0</th><th>0</th><th>0</th><th>0</th><th>0</th><th>0</th><th class="scrollPadding"></th></tr></tbody></table></div>'
	
	
	//Add new tab and table then refresh JqueryUI tabs
	$('#tableContainer').tabs().find(".ui-tabs-nav").append(newTab);
	$('#tableContainer').append(newTable);
	$('#tableContainer').tabs("refresh");
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('buildNewTab');
	return localErrorCode;
}

function generateGrid(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('generateGrid');
		else _STACKTRACE_ = new stackTrace('generateGrid');
	}
	else _STACKTRACE_ = new stackTrace('generateGrid');
	var localErrorCode = -1;
	
	var gridHTML = '';
	var blockArray = ['TL', 'TR', 'BL', 'BR'];
	
	for (var i_ = 1; i_ <= 3; i_++){
		for(var _i = 1; _i <= 3; _i++){
			gridHTML += '<div id="square' + i_ + '_' + _i + '" class="square" loc="square' + i_ + '_' + _i + '">';
			for(var block = 0; block <=3; block++){
				gridHTML += '<section class="block' + blockArray[block] + ' block" loc="block' + blockArray[block] + '"></section>';
			}
			gridHTML += '</div>';
		}
	}
	$('#districtDisplay')[0].innerHTML = gridHTML;
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('generateBuildMenu');
	return localErrorCode; 
}

function generateBuildMenu(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('generateBuildMenu');
		else _STACKTRACE_ = new stackTrace('generateBuildMenu');
	}
	else _STACKTRACE_ = new stackTrace('generateBuildMenu');
	var localErrorCode = -1;
	
	var smallMenu = '<ul>';
	var medWideMenu = '<ul>';
	var medTallMenu = '<ul>';
	var largeMenu = '<ul>';
	
	for(var i in buildings){
		switch(buildings[i].size){
			case 1:
				smallMenu += '<li><div class="building" orentation="N/A" nameInternal="' + buildings[i].nameInternal + '">' + buildings[i].name + '</div></li>';
				break;
			case 2:
				medWideMenu += '<li><div class="building" orentation="wide" nameInternal="' + buildings[i].nameInternal + '">' + buildings[i].name + '</div></li>';
				medTallMenu += '<li><div class="building" orentation="tall" nameInternal="' + buildings[i].nameInternal + '">' + buildings[i].name + '</div></li>';
				break;
			case 4:
				largeMenu += '<li><div class="building" orentation="N/A" nameInternal="' + buildings[i].nameInternal + '">' + buildings[i].name + '</div></li>';
				break;
		}
	}
	
	smallMenu += '</ul>';
	medWideMenu += '</ul>';
	medTallMenu += '</ul>';
	largeMenu += '</ul>';
	
	$('#smallMenu')[0].innerHTML += smallMenu;
	$('#medWideMenu')[0].innerHTML += medWideMenu;
	$('#medTallMenu')[0].innerHTML += medTallMenu;
	$('#largeMenu')[0].innerHTML += largeMenu;
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('generateBuildMenu');
	return localErrorCode;
}