/*
Project lead: Jeff evans
Last edit by Jeff Evans
date: 07/11/2016
*/

//Settings file for application

//List of Kingdom Main properties to ignore when building the main table
var _IGNORE_LIST_KINGDOMMAIN_ = [
	'kingdomName',
	'description',
	'buildPoints',
	'spareGold',
	'controlDC',
	'magicItems',
	'unrest',
	'cities',
	'E', 'S', 'L'
];

//list of items in the dashboard to update.
var _DASHBOARD_LIST_ = [
	'kingdomName',
	'buildPoints',
	'spareGold',
	'controlDC',
	'unrest',
	'cities',
	'E', 'S', 'L'
];
