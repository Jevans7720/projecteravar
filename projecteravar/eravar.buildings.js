/*
Project lead: Jeff evans
Last edit by Jeff Evans
date: 6/26/2016
*/

function buildingList() {
	//This is the building list

	this.academy = {
		'name' : 'Academy',
		'nameInternal' : 'academy',
		'cost' : 52,
		'damaged' : false,
		'size' : 2,
		'link' : null,
		'E' : 2,
		'L' : 2,
		'minor' : 3,
		'medium' : 2,
		'halfs' : ["Caster's Tower", "Library", "Magic Shop"]
	}
	this.alchemist = {
		'name' : 'Alchemist',
		'nameInternal' : 'alchemist',
		'cost' : 18,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'minor' : 1,
		'value' : 1000,
		'houseRequired' : 1
	}
	this.arena = {
		'name' : 'Arena',
		'nameInternal' : 'arena',
		'cost' : 40,
		'damaged' : false,
		'size' : 4,
		'link' : null,
		'S' : 4,
		'halfs' : ["Garrison", "Theater"]
	}
	this.barracks = {
		'name' : 'Barracks',
		'nameInternal' : 'barracks',
		'size' : 1,
		'cost' : 12,
		'damaged' : false,
		'defenceMod' : 2,
		'unrest' : -1
	}
	this.blackMarket = {
		'name' : 'Black Market',
		'nameInternal' : 'blackMarket',
		'cost' : 50,
		'damaged' : false,
		'size' : 1,
		'E' : 2,
		'S' : 1,
		'unrest' : 1,
		'minor' : 2,
		'medium' : 1,
		'major' : 1,
		'value' : 2000,
		'houseRequired' : 2
	}
	this.brewery = {
		'name' : 'Brewery',
		'nameInternal' : 'brewery',
		'cost' : 6,
		'damaged' : false,
		'size' : 1,
		'S' : 1,
		'L' : 1
	}
	this.brothel = {
		'name' : 'Brothel',
		'nameInternal' : 'brothel',
		'cost' : 4,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'L' : 2,
		'unrest' : 1,
		'houseRequired' : 2
	}
	this.castersTower = {
		'name' : "Caster's Tower",
		'nameInternal' : 'castersTower',
		'cost' : 30,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'L' : 1,
		'minor' : 3,
		'medium' : 2,
	}
	this.castle = {
		'name' : 'Castle',
		'nameInternal' : 'castle',
		'cost' : 54,
		'damaged' : false,
		'size' : 4,
		'link' : null,
		'E' : 2,
		'L' : 2,
		'S' : 2,
		'defenceMod' : 2,
		'unrest' : -4,
		'halfs' : ["Noble Villa", "Town Hall"],
		'limit' : 1
	}
	this.cathedral = {
		'name' : 'Cathedral',
		'nameInternal' : 'cathedral',
		'cost' : 58,
		'damaged' : false,
		'size' : 4,
		'link' : null,
		'L' : 4,
		'minor' : 3,
		'medium' : 2,
		'unrest' : -4,
		'halfs': ["Temple", "Academy"],
		'limit' : 1
	}
	this.cityWall = {
		'name' : 'City Wall',
		'nameInternal' : 'cityWall',
		'cost' : 8,
		'damaged' : false,
		'defenceMod' : 4,
		'unrest' : -2,
		'special' : 'Built on land borders only. Does not consume district squares.'
	}
	this.dump = {
		'name' : 'Dump',
		'nameInternal' : 'dump',
		'cost' : 4,
		'damaged' : false,
		'size' : 1,
		'S' : 1,
		'L' : 1,
	}
	this.exoticCraftsman = {
		'name' : 'Exotic Craftsman',
		'nameInternal' : 'exoticCraftsman',
		'cost' : 10,
		'damaged' : false,
		'size' : 1,
		'S' : 1,
		'L' : 1,
		'minor' : 1,
		'houseRequired' : 1
	}
	this.garrison = {
		'name' : 'Garrison',
		'nameInternal' : 'garrison',
		'cost' : 28,
		'damaged' : false,
		'size' : 2,
		'link' : null,
		'S' : 2,
		'L' : 2,
		'unrest' : -2,
		'halfs': ["City Wall", "Granary", "Jail"]
	}
	this.granary = {
		'name' : 'Granary',
		'nameInternal' : 'granary',
		'cost' : 12,
		'damaged' : false,
		'size' : 1,
		'S' : 1,
		'L' : 1,
	}
	this.graveyard = {
		'name' : 'Graveyard',
		'nameInternal' : 'graveyard',
		'cost' : 4,
		'damaged' : false,
		'E' : 1,
		'L' : 1,
	}
	this.guildhall = {
		'name' : 'Guildhall',
		'nameInternal' : 'guildhall',
		'cost' : 34,
		'damaged' : false,
		'size' : 2,
		'link' : null,
		'E' : 2,
		'L' : 2,
		'value' : 1000,
		'halfs' : ["Pier", "Stable", "Tradesman"],
		'houseRequired' : 1
	}
	this.herbalist = {
		'name' : 'Herbalist',
		'nameInternal' : 'herbalist',
		'cost' : 10,
		'damaged' : false,
		'size' : 1,
		'S' : 1,
		'L' : 1,
		'minor' : 1,
		'houseRequired' : 1
	}
	this.house = {
		'name' : 'House',
		'nameInternal' : 'house',
		'size' : 1,
		'cost' : 3,
		'damaged' : false,
		'unrest' : -1,
	}
	this.inn = {
		'name' : 'Inn',
		'nameInternal' : 'inn',
		'cost' : 10,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'L' : 1,
		'value' : 500,
		'houseRequired' : 1
	}
	this.jail = {
		'name' : 'Jail',
		'nameInternal' : 'jail',
		'cost' : 14,
		'damaged' : false,
		'size' : 1,
		'S' : 2,
		'L' : 2,
		'unrest' : -2,
	}
	this.library = {
		'name' : 'Library',
		'nameInternal' : 'library',
		'cost' : 6,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'L' : 1,
	}
	this.luxuryStore = {
		'name' : 'Luxury Store',
		'nameInternal' : 'luxuryStore',
		'cost' : 28,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'minor' : 2,
		'value' : 2000,
		'houseRequired' : 1
	}
	this.magicShop = {
		'name' : 'Magic Shop',
		'nameInternal' : 'magicShop',
		'cost' : 68,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'minor' : 4,
		'medium' : 2,
		'major' : 1,
		'value' : 2000,
		'houseRequired' : 2
	}
	this.masnion = {
		'name' : 'Mansion',
		'nameInternal' : 'masnion',
		'cost' : 10,
		'damaged' : false,
		'size' : 1,
		'S' : 1,
	}
	this.market = {
		'name' : 'Market',
		'nameInternal' : 'market',
		'cost' : 48,
		'damaged' : false,
		'size' : 2,
		'link' : null,
		'E' : 2,
		'S' : 2,
		'minor' : 2,
		'value' : 2000,
		'halfs' : ["Black Market", "Inn", "Shop"],
		'houseRequired' : 2
	}
	this.mill = {
		'name' : 'Mill',
		'nameInternal' : 'mill',
		'cost' : 6,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'S' : 1,
		'special' : 'Must be adjacent to a water border.'
	}
	this.monument = {
		'name' : 'Monument',
		'nameInternal' : 'monument',
		'cost' : 6,
		'damaged' : false,
		'size' : 1,
		'L' : 3,
		'unrest' : -1,
	}
	this.nobleVilla = {
		'name' : 'Noble Villa',
		'nameInternal' : 'nobleVilla',
		'cost' : 24,
		'damaged' : false,
		'size' : 2,
		'link' : null,
		'E' : 1,
		'L' : 1,
		'S' : 1,
		'halfs' : ["Exotic Craftsman", "Luxury Store", "Mansion"]
	}
	this.park = {
		'name' : 'Park',
		'nameInternal' : 'park',
		'cost' : 4,
		'damaged' : false,
		'size' : 1,
		'L' : 1,
		'unrest' : -1,
	}
	this.pier = {
		'name' : 'Pier',
		'nameInternal' : 'pier',
		'cost' : 16,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'S' : 1,
		'value' : 1000,
		'special' : 'Must be adjacent to a water border.'
	}
	this.shop = {
		'name' : 'Shop',
		'nameInternal' : 'shop',
		'cost' : 8,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'value' : 500,
		'houseRequired' : 1
	}
	this.shrine = {
		'name' : 'Shrine',
		'nameInternal' : 'shrine',
		'cost' : 8,
		'damaged' : false,
		'size' : 1,
		'L' : 1,
		'unrest' : -1,
	}
	this.smith = {
		'name' : 'Smith',
		'nameInternal' : 'smith',
		'cost' : 4,
		'damaged' : false,
		'size' : 1,
		'L' : 1,
		'unrest' : -1,
	}
	this.stable = {
		'name' : 'Stable',
		'nameInternal' : 'stable',
		'cost' : 10,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'L' : 1,
		'value' : 500,
		'houseRequired' : 1
	}
	this.tannery = {
		'name' : 'Tannery',
		'nameInternal' : 'tannery',
		'cost' : 6,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'L' : 1,
		'houseRequired' : 1
	}
	this.tavern = {
		'name' : 'Tavern',
		'nameInternal' : 'tavern',
		'cost' : 12,
		'damaged' : false,
		'size' : 1,
		'E' : 1,
		'L' : 1,
		'value' : 500,
		'houseRequired' : 1
	}
	this.temple = {
		'name' : 'Temple',
		'nameInternal' : 'temple',
		'cost' : 32,
		'damaged' : false,
		'size' : 2,
		'link' : null,
		'S' : 2,
		'L' : 2,
		'unrest' : -2,
		'minor' : 2,
		'halfs' : ["Graveyard", "Monument", "Shrine"]
	}
	this.tenement = {
		'name' : 'Tenement',
		'nameInternal' : 'tenement',
		'cost' : 1,
		'damaged' : false,
		'size' : 1,
		'unrest' : 2,
	}
	this.theater = {
		'name' : 'Theater',
		'nameInternal' : 'theater',
		'cost' : 24,
		'damaged' : false,
		'size' : 2,
		'link' : null,
		'E' : 2,
		'S' : 2,
		'halfs' : ["Brothel", "Park", "Tavern"]
	}
	this.townHall = {
		'name' : 'Town Hall',
		'nameInternal' : 'townHall',
		'cost' : 22,
		'damaged' : false,
		'size' : 2,
		'link' : null,
		'E' : 1,
		'S' : 1,
		'L' : 1,
		'halfs' : ["Barracks", "Dump", "Watch Tower"]
	}
	this.tradesman = {
		'name' : 'Tradesman',
		'nameInternal' : 'tradesman',
		'cost' : 10,
		'damaged' : false,
		'E' : 1,
		'S' : 1,
		'value' : 500,
		'houseRequired' : 1
	}
	this.watchTower = {
		'name' : 'Watch Tower',
		'nameInternal' : 'watchTower',
		'cost' : 6,
		'damaged' : false,
		'size' : 1,
		'S' : 1,
		'defenceMod' : 2,
		'unrest' : -1,
	}
	this.waterfront = {
		'name' : 'Waterfront',
		'nameInternal' : 'Waterfront',
		'cost' : 90,
		'damaged' : false,
		'size' : 4,
		'link' : null,
		'E' : 4,
		'minor' : 3,
		'medium' : 2,
		'major' : 1,
		'value' : 4000,
		'halfs' : ["Guildhall", "Market"]
	}
}