/*
Project lead: Jeff evans
Last edit by Jeff Evans
date: 7/16/2016
*/

function initializeApp(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('initializeApp');
		else _STACKTRACE_ = new stackTrace('initializeApp');
	}
	else _STACKTRACE_ = new stackTrace('initializeApp');
	var localErrorCode = -1;

	buildMainTemplate(_STACKTRACE_);
	generateGrid(_STACKTRACE_);
	generateBuildMenu(_STACKTRACE_)



	//Initialize Misc JqueryUI elements

	$('.cancelButton').button().click(function(){
		//Reset edict overlay.
		$('#edictName')[0].selectedIndex = 0;
		$('#edictName').selectmenu('refresh');
		$('#edictLevel [value=1]').html('Token');
		$('#edictLevel [value=2]').html('Standard');
		$('#edictLevel [value=3]').html('Aggresive');
		$('#edictLevel [value=4]').html('Expansionist');
		$('#edictLevel').selectmenu('refresh');
		$('#edictWindow .applyButton').attr('edictName', 'promotion');
		$('#edictWindow .applyButton').attr('edictLevel', '0');
		//Reset role overlay
		$('#roleSelector')[0].selectedIndex = 0;
		$('#roleSelector').selectmenu('refresh');
		$('#elsContainer input').checkboxradio('enable');
		$('#elsContainer input')[0].checked = false;
		$('#elsContainer input')[1].checked = false;
		$('#elsContainer input')[2].checked = false;
		$('#elsContainer input').checkboxradio('refresh');
		$('#name').val('');
		$('#modifier').val('');
		$('#roleWindow #modifier').attr('placeholder', $('option:contains(Ruler)').attr('mod') + ' Mod');
		$('#roleWindowApply').attr('kingdomRole', 'ruler');
		//Reset load overlay
		$('#loadButton').attr('saveFile', '');
		$('#saveFileExternal')[0].value = null;
		$('#browseButtonFix')[0].value = 'No file Selected';
		$('#saveFile')[0].selectedIndex = 0;
		$('#saveFile').selectmenu('refresh');
		$('#browseButtonFix').addClass('ui-state-disabled')
		$('#saveFileExternal').attr('disabled',"");
		//
		hideOverlay(_STACKTRACE_);
	});

	$(function() {
			$("#tableContainer").tabs();
	});

	//Load window events and UI
	$('#saveFile').selectmenu({
		change: function(event){
			$('#loadButton').attr('saveFile', event.target.value);
			if(event.target.value == 'upload.json'){
				$('#browseButtonFix').removeClass('ui-state-disabled')
				$('#saveFileExternal').removeAttr('disabled');
			}
			else {
				$('#browseButtonFix').addClass('ui-state-disabled')
				$('#saveFileExternal').attr('disabled',"");
			}
		}
	})
	
	$('#saveFileExternal').change(function(event){
		$('#browseButtonFix').attr('value', event.target.files[0].name);
		readFile.readAsText($('#saveFileExternal')[0].files[0]);
	});

	$('#loadButton').button().click(function(){
		getJSON($('#loadButton').attr('saveFile'), _STACKTRACE_);
		$('#loadButton').attr('saveFile', '');
		$('#saveFileExternal')[0].value = null;
		$('#browseButtonFix')[0].value = 'No file Selected';
		$('#saveFile')[0].selectedIndex = 0;
		$('#saveFile').selectmenu('refresh');
		$('#browseButtonFix').addClass('ui-state-disabled')
		$('#saveFileExternal').attr('disabled',"");
		hideOverlay(_STACKTRACE_);
	});

	//Role overlay JqueryUI
	{
		$('#roleSelector').selectmenu({
			change: function(event) {
				var role = $('option:contains(' + event.target.value + ')').attr('internal');
				var stats = $('option[internal=' + role + ']').attr('stat');
				$('#roleWindow #modifier').attr('placeholder', $('option:contains(' + event.target.value + ')').attr('mod') + ' Mod');
				$('#roleWindowApply').attr('kingdomRole', role);
				switch(role){
					case 'ruler':
					case 'rulerSpouse':
					case 'spymaster':
						$('#elsContainer input').checkboxradio('enable');
						$('#elsContainer input')[0].checked = false;
						$('#elsContainer input')[1].checked = false;
						$('#elsContainer input')[2].checked = false;
						$('#elsContainer input').checkboxradio('refresh');
						break;
					default:
						$('#elsContainer input')[0].checked = false;
						$('#elsContainer input')[1].checked = false;
						$('#elsContainer input')[2].checked = false;
						$('#role' + stats)[0].checked = true;
						$('#elsContainer input').checkboxradio('disable');
						$('#elsContainer input').checkboxradio('refresh');
						break;
				}
			}
		});
		$('#roleWindowClose').button({icons: {primary: "ui-icon-closethick"},text: false});
		$('#roleWindowApply').button().click(function(event){
			var role = $('#roleWindowApply').attr('kingdomRole');
			var name = $("#roleWindow #name")[0].value;
			var mod = parseInt($("#roleWindow #modifier")[0].value);
			var stats = [];
			var limit = $('#elsContainer input:checked').length;
			switch(role){
				case 'ruler':
				case 'rulerSpouse':
					for(var box = 0; box < limit; box++){
						_STACKTRACE_.addToStack(box);
						stats.push($('#elsContainer input:checked')[box].name);
						console.log(stats);
						_STACKTRACE_.removeFromStack(box);
					}
					break;
				case 'spymaster':
					if($('#elsContainer input:checked').length > 1){
						alert('Too many option selected for this role');
						return;
					}
					else stats.push($('#elsContainer input:checked')[0].name);
					break;
				default:
					stats.push($('#elsContainer input:checked')[0].name);
					break;
			}
			updateRoles(role, name, mod, stats, _STACKTRACE_)
			//reset overlay
			$('#roleSelector')[0].selectedIndex = 0;
			$('#roleSelector').selectmenu('refresh');
			$('#elsContainer input').checkboxradio('enable');
			$('#elsContainer input')[0].checked = false;
			$('#elsContainer input')[1].checked = false;
			$('#elsContainer input')[2].checked = false;
			$('#elsContainer input').checkboxradio('refresh');
			$('#name').val('');
			$('#modifier').val('');
			$('#roleWindow #modifier').attr('placeholder', $('option:contains(Ruler)').attr('mod') + ' Mod');
			$('#roleWindowApply').attr('kingdomRole', 'ruler');
			
			hideOverlay(_STACKTRACE_);
		});
		
		$('#elsContainer input').checkboxradio({
      icon: false
    });
	}
	
	//Edit Edict window events and UI
	$('#edictName').selectmenu({
		change: function(event){
			$('#edictWindow .applyButton').attr('edictName', event.target.value);
			if(event.target.value == 'promotion'){
				$('#edictLevel [value=1]').html('Token');
				$('#edictLevel [value=2]').html('Standard');
				$('#edictLevel [value=3]').html('Aggresive');
				$('#edictLevel [value=4]').html('Expansionist');
			}
			if(event.target.value == 'taxation'){
				$('#edictLevel [value=1]').html('Light');
				$('#edictLevel [value=2]').html('Normal');
				$('#edictLevel [value=3]').html('Heavy');
				$('#edictLevel [value=4]').html('Overwhelming');
			}
			if(event.target.value == 'festivals'){
				$('#edictLevel [value=1]').html('1 per year');
				$('#edictLevel [value=2]').html('6 per year');
				$('#edictLevel [value=3]').html('12 per year');
				$('#edictLevel [value=4]').html('24 per year');
			}
			$('#edictLevel').selectmenu('refresh');
		}
	});
	$('#edictLevel').selectmenu({
		change: function(event){
			$('#edictWindow .applyButton').attr('edictLevel', event.target.value);
		}
	});

	$('#edictWindow .applyButton').button().click(function(){
		updateEdicts($('#edictWindow .applyButton').attr('edictName'), parseInt($('#edictWindow .applyButton').attr('edictLevel')), _STACKTRACE_);
		$('#edictName')[0].selectedIndex = 0;
		$('#edictName').selectmenu('refresh');
		$('#edictLevel [value=1]').html('Token');
		$('#edictLevel [value=2]').html('Standard');
		$('#edictLevel [value=3]').html('Aggresive');
		$('#edictLevel [value=4]').html('Expansionist');
		$('#edictLevel').selectmenu('refresh');
		$('#edictWindow .applyButton').attr('edictName', 'promotion');
		$('#edictWindow .applyButton').attr('edictLevel', '0');
		hideOverlay(_STACKTRACE_);
	});
	
	//New district window events and UI
	$('#newDistrictCity').selectmenu({
		change: function(event){
			$('#newDistrictWindow .applyButton').attr('newDistrictCity', event.target.value);
		}
	});
	$('#newDistrictTerrain').selectmenu({
		change: function(event){
			$('#newDistrictWindow .applyButton').attr('newDistrictTerrain', event.target.value);
		}
	});

	$('#newDistrictWindow .applyButton').button().click(function(){
		buildNewDistrict($('#newDistrictWindow .applyButton').attr('newDistrictCity'), $('#newDistrictWindow .applyButton').attr('newDistrictTerrain'), _STACKTRACE_);
		$('#newDistrictCity')[0].selectedIndex = 0;
		$('#newDistrictCity').selectmenu('refresh');
		$('#newDistrictTerrain')[0].selectedIndex = 0;
		$('#newDistrictTerrain').selectmenu('refresh');
		$('#newDistrictWindow .applyButton').attr('newDistrictCity', 'null');
		$('#newDistrictWindow .applyButton').attr('newDistrictTerrain', 'grassland');
		hideOverlay(_STACKTRACE_);
	});

	//Initialize JqueryUI header
	{
		$('#mainMenu').menu();
		$('#depositGold').button({disabled: true,});
		$('#advanceMonth').button({disabled: true,});
		$('#monthAmount').spinner({
			min: 1,
			max: 12,
			step: 1,
			start: 1,
			disabled: true,
		});
		$('#goldAmount').val(0);
		$('#monthAmount').val(1);
		$('#kingdomName').val('No kingdom Loaded');
		$('#kingdomName')[0].onkeyup = function(){
			kingdom.main.kingdomName = $('#kingdomName')[0].value;
		}

		$('#menuIconContainer').click(function(){
		$('#mainMenuContainer').toggleClass('visible');
		});
		$('#mainMenuContainer').mouseleave(function(){
			$('#mainMenuContainer').removeClass('visible');
		});
	}

	//District view events and JQuery UI
	$('#districtCitySelect').selectmenu({
		change: function(event) {
			var city = $('option[value=' + event.target.value + ']').html();
			if(city == 'Capital')city = city.toLowerCase();
			$('#districtSelect').attr('cityName', city);
			$('#districtDistrictSelect').html('<option value="null" disabled selected>ID</option><option value="1">1</option>');
			console.log(kingdom[city]);
			for(var district in kingdom[city].districts){
				if(district == 0 || district == 1) continue;
				$('#districtDistrictSelect').html($('#districtDistrictSelect').html() + '<option value="' + district + '">' + district + '</option>');
			}
			$('#districtDistrictSelect').selectmenu('refresh');
		}
	});

	$('#districtDistrictSelect').selectmenu({
		change: function(event) {
			var ID = $('#districtDistrictSelect option[value=' + event.target.value + ']').html();
			$('#districtSelect').attr('districtID', ID);
		}
	});

	$('#districtSelect').button();
	$('#districtSelect').click(function(event){
		if($('#districtSelect').attr('cityName') === ''){alert('No city selected.');return}
		if($('#districtSelect').attr('districtID') === ''){alert('No city selected.');return}
		populateDistWindow($('#districtSelect').attr('cityName'), $('#districtSelect').attr('districtID'), _STACKTRACE_);
	});

	$('#districtDistrictSelect').selectmenu();

	$('.block').click(function(event){
		var block = event.target.attributes.loc.value;
		var square = event.target.parentElement.attributes.loc.value;
		if(event.target.innerHTML == 'Empty'){
			if($("#districtOptionMenu").css('display') != 'none')$("#districtOptionMenu").hide('blind', {}, 750);
			if($("#districtBuildMenu").css('display') == 'none'){
				$('#districtBuildMenu').attr('block', block);
				$('#districtBuildMenu').attr('square', square);
				prettifyDistrictGrid(square, block, 'Build', _STACKTRACE_);
				$("#districtBuildMenu").show('blind', {}, 750);
			}
			else{
				if($('#districtBuildMenu').attr('block') == block && $('#districtBuildMenu').attr('square') == square){
					$("#districtBuildMenu").hide('blind', {}, 750);
				}
				else{
					$('#districtBuildMenu').attr('block', block);
					$('#districtBuildMenu').attr('square', square);
					$('#districtBuildMenu').hide('blind', {}, 200, function(){prettifyDistrictGrid(square, block, 'Build', _STACKTRACE_)});
					$('#districtBuildMenu').show('blind', {}, 200);
				}
			}
		}
		else{
			if($("#districtBuildMenu").css('display') != 'none')$("#districtBuildMenu").hide('blind', {}, 750);
			if($("#districtOptionMenu").css('display') == 'none'){
				$('#districtOptionMenu').attr('block', block);
				$('#districtOptionMenu').attr('square', square);
				prettifyDistrictGrid(square, block, 'Option', _STACKTRACE_);
				$("#districtOptionMenu").show('blind', {}, 750);
			}
			else{
				if($('#districtOptionMenu').attr('block') == block && $('#districtOptionMenu').attr('square') == square){
					$("#districtOptionMenu").hide('blind', {}, 750);
				}
				else{
					$('#districtOptionMenu').attr('block', block);
					$('#districtOptionMenu').attr('square', square);
					$('#districtOptionMenu').hide('blind', {}, 200, function(){prettifyDistrictGrid(square, block, 'Option', _STACKTRACE_)});
					$('#districtOptionMenu').show('blind', {}, 200);
				}
			}
		}
	});

	$('.building').click(function(event){
		var city = $('#districtDisplay').attr('city');
		var dist = $('#districtDisplay').attr('district');

		var square = $('#districtBuildMenu').attr('square');
		var block = $('#districtBuildMenu').attr('block');

		var buildingName = event.target.attributes.nameInternal.value;
		var orientation = event.target.attributes.orentation.value;

		buildNewBuilding(kingdom[city].districts[dist], square, block, buildingName, orientation, _STACKTRACE_);
	});

	$('#damage').click(function(event){
		var district = kingdom[$('#districtDisplay').attr('city')].districts[$('#districtDisplay').attr('district')];
		var square = $('#districtOptionMenu').attr('square');
		var block = $('#districtOptionMenu').attr('block');

		if(district.buildings[square][block].damaged === true){
			alert('Building is already marked as damaged');
			return;
		}

		damageBuilding(district, square, block, _STACKTRACE_, false);
		refreshDistrictWindow(_STACKTRACE_);
		updateCityTable($('#districtDisplay').attr('city'), _STACKTRACE_);
		alert('Building has been marked as damaged');
	});

	$('#repair').click(function(event){
		var district = kingdom[$('#districtDisplay').attr('city')].districts[$('#districtDisplay').attr('district')];
		var square = $('#districtOptionMenu').attr('square');
		var block = $('#districtOptionMenu').attr('block');

		if(district.buildings[square][block].damaged === false){
			alert('Building is not marked as damaged');
			return;
		}

		repairBuilding(district, square, block, _STACKTRACE_, false);
		refreshDistrictWindow(_STACKTRACE_);
		updateCityTable($('#districtDisplay').attr('city'), _STACKTRACE_);
		alert('Building has been repaired');
	});

	//Initailize JqueryUI elements for district window
	$('#districtBuildMenu').menu();
	$('#districtOptionMenu').menu();

	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('initializeApp');
	return localErrorCode;
}


function bindMenuEvents(_STACKTRACE_){
	$('#mainMenu [function]').unbind()
	
	$('#mainMenu [function]:not(.ui-state-disabled)').click(function(event){
		switch(event.target.parentElement.attributes['function'].value){
			case 'load':
				showOverlay('loadContainer', _STACKTRACE_);
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			case 'save':
				saveJSON(_STACKTRACE_);
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			case 'table':
				$('#tableContainer').addClass('visible');
				$('#districtContainer').removeClass('visible');
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			case 'district':
				$('#tableContainer').removeClass('visible');
				$('#districtContainer').addClass('visible');
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			//////////////////////////////////////////////////////
				//Edit Kingdom Sub menu
			//////////////////////////////////////////////////////
			case 'roleEdit':
				showOverlay('roleContainer', _STACKTRACE_);
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			case 'edictEdit':
				showOverlay('roleContainer', _STACKTRACE_);
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			case 'newDistrict':
				showOverlay('roleContainer', _STACKTRACE_);
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			//////////////////////////////////////////////////////
				//End submenu
			//////////////////////////////////////////////////////
				
			//////////////////////////////////////////////////////
				//Testing Fucntions Submenu
			//////////////////////////////////////////////////////
			case 'update':
				updateKingdom(_STACKTRACE_);
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			case 'clear':
				$('#kingdom').html('');$('#save').html('');
				$('#mainMenuContainer').removeClass('visible');
				break;
				
			//////////////////////////////////////////////////////
				//End submenu
			//////////////////////////////////////////////////////
			case 'about':
				printVersion(_STACKTRACE_);
				$('#mainMenuContainer').removeClass('visible');
				break;
		}
	});
}