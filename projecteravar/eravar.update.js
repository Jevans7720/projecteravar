/*
Project lead: Jeff Evans
Last edit by Jeff Evans
date: 07/15/2016
*/

//This is the update file. All functions the relate to updating kingdom data reside here

function updateKingdom(_STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('updateKingdom');
		else _STACKTRACE_ = new stackTrace('updateKingdom');
	}
	else _STACKTRACE_ = new stackTrace('updateKingdom');
	var localErrorCode = -1;
	
	showOverlay('throbberContainer', _STACKTRACE_);
	
	//Total the kingdom and update the main table
	totalKingdom(_STACKTRACE_);
	updateUI(_STACKTRACE_);
	
	//Total each city in the kingdom 
	setTimeout(function(){hideOverlay(_STACKTRACE_);}, 2000);
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('updateKingdom');
	return localErrorCode;
}

function totalCity(cityObj, _STACKTRACE_) {
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('totalCity');
		else _STACKTRACE_ = new stackTrace('totalCity');
	} else _STACKTRACE_ = new stackTrace('totalCity');
	var localErrorCode = -1;
	
	//Variable to hold blocks to skip for buildings of size 2
	var skip = '';
	//Variable to tell if a square needs to be skipped for a building of size 4
	var skipBlock = false;

	//0 all values before totaling
	for (var property in cityObj) {
		_STACKTRACE_.addToStack(property);
		switch (property) {
			case 'E':
			case 'S':
			case 'L':
			case 'defenceMod':
			case 'value':
				cityObj[property] = 0;
				break;
			case 'magicItems':
				cityObj[property].majorLimit = 0;
				cityObj[property].mediumLimit = 0;
				cityObj[property].minorLimit = 0;
				break;
			default:
				_STACKTRACE_.removeFromStack(property);
				continue;
		}
		_STACKTRACE_.removeFromStack(property);
	}
	
	breakout:{
		//Check to see if the city has districts, otherwise throw error
		if (cityObj.districts === undefined){localErrorCode = 111; break breakout;}
		//Make sure districts are objects otherwise throw error
		if (typeof cityObj.districts != 'object'){localErrorCode = 112; break breakout;}
		//If all checks succeed loop through districts
		for (var id in cityObj.districts) {
			if (id == '0') continue;
			_STACKTRACE_.addToStack(id);
			
			//Make sure the district in question is defined otherwise throw error
			if (cityObj.districts[id] === undefined){localErrorCode = 111; printError(localErrorCode, _STACKTRACE_, [id], false); _STACKTRACE_.removeFromStack(id);break;}
			//Make sure the district in question is an object otherwise throw error
			if (typeof cityObj.districts[id] != 'object'){localErrorCode = 112; printError(localErrorCode, _STACKTRACE_, [id], false); _STACKTRACE_.removeFromStack(id);break;}
			//If all checks succeed loop through buildings
			
			//loop through blocks
			for (var square in cityObj.districts[id].buildings) {
				_STACKTRACE_.addToStack(square);
				skipBlock = false;
				for (var building in cityObj.districts[id].buildings[square]) {
					_STACKTRACE_.addToStack(building);
					
					if(building == skip){skip = '';_STACKTRACE_.removeFromStack(building);continue;}
					if(skipBlock === true){_STACKTRACE_.removeFromStack(building);break;}
					
					//Make sure the building in question is defined otherwise throw error
					if (cityObj.districts[id].buildings[square][building] === undefined) {localErrorCode = 111; printError(localErrorCode, _STACKTRACE_, [cityObj.districts[id].buildings[square][building]], false); _STACKTRACE_.removeFromStack(building);break;}
					
					//check if the block in question is empty. if so skip over it.
					if (cityObj.districts[id].buildings[square][building] == "Empty"){
						console.log('Building empty, not added to total');
						_STACKTRACE_.removeFromStack(building);
						continue;
					}
					
					//Make sure the building in question is an object otherwise throw error
					if (typeof cityObj.districts[id].buildings[square][building] != 'object') {localErrorCode = 112; printError(localErrorCode, _STACKTRACE_, [cityObj.districts[id].buildings[square][building]], false); _STACKTRACE_.removeFromStack(building);break;}
					
					if (cityObj.districts[id].buildings[square][building].damaged === undefined){
						cityObj.districts[id].buildings[square][building].damaged = false;
					}
					
					//check to see if the building is damaged, if so do not add to the total.
					if (cityObj.districts[id].buildings[square][building].damaged !== false) {
						console.log('Building damaged, not added to total');
						_STACKTRACE_.removeFromStack(building);
						continue;
					}
					var testAgainst = buildings[cityObj.districts[id].buildings[square][building].nameInternal];
					
					//Check to see if the building currently being checked matches the buildings list if not throw error
					if (testAgainst === undefined) {localErrorCode = 113; printError(localErrorCode, _STACKTRACE_, [testAgainst], false); _STACKTRACE_.removeFromStack(building);break;}
					//For now this will just throw an error. Later it will also check a list of custom buildings
					
					//Loop through all prperties that can be totalled
					for (var property in testAgainst) {
						_STACKTRACE_.addToStack(property);
						var objProperty = cityObj.districts[id].buildings[square][building][property];
						switch (property) {
							case 'E':
							case 'S':
							case 'L':
							case 'defenceMod':
							case 'value':
								//Check to see if the value of the property is value, if so add to total, otherwise throw error
								if (objProperty === undefined || typeof objProperty != 'number') {localErrorCode = 114; printError(localErrorCode, _STACKTRACE_, [objProperty], false); break;}
								cityObj[property] += objProperty;
								break;
							case 'minor':
							case 'medium':
							case 'major':
								//Same as above but adjusted to add to the magic item array in the city object.
								if (objProperty === undefined || typeof objProperty != 'number') {localErrorCode = 114; printError(localErrorCode, _STACKTRACE_, [objProperty], false); break;}
								cityObj.magicItems[property + 'Limit'] += objProperty;
								break;
							default:
								_STACKTRACE_.removeFromStack(property);
								continue;
						}
						_STACKTRACE_.removeFromStack(property);
						if (localErrorCode > 100) break;
					}
					if (localErrorCode > 100){
						_STACKTRACE_.removeFromStack(building);
						break;
					}
					
					//if the building just checked is larger than 1 square set variables to skip the next block or square with that building.
					if(cityObj.districts[id].buildings[square][building].link != null)skip = cityObj.districts[id].buildings[square][building].link;
					if(cityObj.districts[id].buildings[square][building].size == 4)skipBlock = true;
					_STACKTRACE_.removeFromStack(building);
				}
				_STACKTRACE_.removeFromStack(square);	
				if (localErrorCode > 100) break;
			}
			_STACKTRACE_.removeFromStack(id);
			if (localErrorCode > 100) break;
		}
	}
	
	if (localErrorCode == -1) localErrorCode = 0;
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('totalCity');
	return localErrorCode;
}

function totalKingdom(_STACKTRACE_) {
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('totalKingdom');
		else _STACKTRACE_ = new stackTrace('totalKingdom');
	} else _STACKTRACE_ = new stackTrace('totalKingdom');
	var localErrorCode = -1;

	breakout:{
		
		if (kingdom.capital === undefined || kingdom.capital.__proto__.constructor != kingdomCity) {localErrorCode = 114; break breakout;}
		
		if (kingdom.main === undefined || kingdom.main.__proto__.constructor != kingdomMain) {localErrorCode = 115; break breakout;}
		
	//0 all properties to be updated.
		for (var property in kingdom.main) {
			_STACKTRACE_.addToStack(property);
			switch (property) {
				case 'E':
				case 'S':
				case 'L':
				case 'cities':
					kingdom.main[property] = 0;
					break;
				case 'magicItems':
					kingdom.main[property].majorLimit = 0;
					kingdom.main[property].mediumLimit = 0;
					kingdom.main[property].minorLimit = 0;
					break;
				default:
					_STACKTRACE_.removeFromStack(property);
					continue;
			}
			_STACKTRACE_.removeFromStack(property);
		}
	
		
		//If all checks succeed loop through districts
		for (var city in kingdom) {
			_STACKTRACE_.addToStack(city);
			switch (city) {
				case 'main':
					for (var property in kingdom.main) {
						if (kingdom.main[property].__proto__.constructor != Object) continue;
						_STACKTRACE_.addToStack(property);
						switch (property) {
							case 'magicItems':
								_STACKTRACE_.removeFromStack(property);
								continue;
							default:
								if (kingdom.main[property] === undefined || kingdom.main[property].__proto__.constructor != Object) {localErrorCode = 115; printError(localErrorCode, _STACKTRACE_, [property, kingdom.main[property]], false); break;}
								
								if (kingdom.main[property].name == 'unoccupied') break;
								//later this will apply penalties
								
								if (kingdom.main[property].E === undefined || typeof kingdom.main[property].E != 'number') {localErrorCode = 115; printError(localErrorCode, _STACKTRACE_, [property, kingdom.main[property].E], false); break;}
								else kingdom.main.E += kingdom.main[property].E;
								
								if (kingdom.main[property].S === undefined || typeof kingdom.main[property].S != 'number') {localErrorCode = 115; printError(localErrorCode, _STACKTRACE_, [property, kingdom.main[property].S], false); break;}
								else kingdom.main.S += kingdom.main[property].S;
								
								if (kingdom.main[property].L === undefined || typeof kingdom.main[property].L != 'number') {localErrorCode = 115; printError(localErrorCode, _STACKTRACE_, [property, kingdom.main[property].L], false); break;}
								else kingdom.main.L += kingdom.main[property].L;
						}
						if (localErrorCode > 100) break;
						_STACKTRACE_.removeFromStack(property);
					}
					break;
				case 'temp':
					_STACKTRACE_.removeFromStack(city);
					continue;
				default:
					if (kingdom[city].__proto__.constructor != kingdomCity) {
						localErrorCode = 20;
						printError(localErrorCode, _STACKTRACE_, [city, kingdom[city]], true);
					}
					localErrorCode = totalCity(kingdom[city], _STACKTRACE_);
					printError(localErrorCode, _STACKTRACE_, [city, kingdom[city]], false);
					if (localErrorCode > 100) break;
					else {
						kingdom.main.cities += 1;
						for (var property in kingdom[city]) {
							_STACKTRACE_.addToStack(property);
							switch (property) {
								case 'E':
								case 'S':
								case 'L':
									if (kingdom[city][property] === undefined || typeof kingdom[city][property] != 'number') {localErrorCode = 114; printError(localErrorCode, _STACKTRACE_, [city, property, kingdom[city][property]], false); break;}
									
									kingdom.main[property] += kingdom[city][property];
									break;
								case 'magicItems':
									if (kingdom[city][property] === undefined || kingdom[city][property].__proto__.constructor != Object) { localErrorCode = 114; printError(localErrorCode, _STACKTRACE_, [city, property, kingdom[city][property]], false); break;}
									
									kingdom.main[property].majorLimit += kingdom[city][property].majorLimit;
									kingdom.main[property].mediumLimit += kingdom[city][property].mediumLimit;
									kingdom.main[property].minorLimit += kingdom[city][property].minorLimit;
									break;
								default:
									_STACKTRACE_.removeFromStack(property);
									continue;
							}
							_STACKTRACE_.removeFromStack(property);
						}
						if (localErrorCode > 100) break;
					}
			}
			_STACKTRACE_.removeFromStack(city);
			if (localErrorCode > 100) break;
		}
	}
	if (localErrorCode == -1) localErrorCode = 0;
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('totalKingdom');
	return localErrorCode;
}

function depositBuildPoints(gold, _STACKTRACE_) {
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('depositBuildPoints');
		else _STACKTRACE_ = new stackTrace('depositBuildPoints');
	} else _STACKTRACE_ = new stackTrace('depositBuildPoints');
	var localErrorCode = -1;
	
	breakout:{
		//If gold is 0 or lower throw error
		if (gold <= 0) {localErrorCode = 191; break breakout;}
		//If gold is not a number throw error
		if (typeof gold != 'number' || isNaN(gold)) {localErrorCode = 192; break breakout;}
		
		//if main is undefined throw error and escape
		if (kingdom.main === undefined || kingdom.main.__proto__.constructor != kingdomMain) {localErrorCode = 115; break breakout;}
		
		//kingdom.main.buildPoints += Math.floor(gold / 4000);
		kingdom.main.update('buildPoints', kingdom.main.buildPoints + Math.floor(gold / 4000), _STACKTRACE_);
		//kingdom.main.spareGold += Math.floor(gold % 4000);
		kingdom.main.update('buildPoints', kingdom.main.buildPoints + Math.floor(gold % 4000), _STACKTRACE_);
		
		//tell the user how many built points / extra gold has been added
		alert(Math.floor(gold / 4000) + " build points added. \n" + Math.floor(gold % 4000) + " spare gold added.")
		
		//0 the amount of gold in the dashboard then update the dashboard
		$('#goldAmount').val(0);
		updateDash(_STACKTRACE_);
	}

	if (localErrorCode == -1) localErrorCode = 0;
	printError(localErrorCode, _STACKTRACE_, [gold], true);
	_STACKTRACE_.removeFromStack('depositBuildPoints');
	return localErrorCode;
}

function damageBuilding(districtObject, square, block, _STACKTRACE_, fromLink) {
	console.log('formLink = ' + fromLink);
	fromLink = fromLink || false;
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('damageBuilding');
		else _STACKTRACE_ = new stackTrace('damageBuilding');
	} 
	else _STACKTRACE_ = new stackTrace('damageBuilding');
	var localErrorCode = -1;
	
	breakout:{
		//Check the object path to make sure nothing is undefined
		if (districtObject === undefined || districtObject.__proto__.constructor != cityDistrict) {localErrorCode = 103; break breakout;}
		if (districtObject.buildings[square] === undefined || districtObject.buildings[square].__proto__.constructor != Object) {localErrorCode = 103; break breakout;}
		if (districtObject.buildings[square][block] === undefined || districtObject.buildings[square][block].__proto__.constructor != Object) {localErrorCode = 103; break breakout;}
		
		//Update the buildings damaged value
		localErrorCode = districtObject.updateBuilding(square, block, 'damaged', true, _STACKTRACE_);

		//Prevent infinite recursion by passing over if called from a different damageBuilding instance
		if(fromLink === false ){
			//If the building take up multiple blocks rerun the function for each block the building takes up
			if(districtObject.buildings[square][block].size == 2){
				var checkReturn = damageBuilding(districtObject, square, districtObject.buildings[square][block].link, _STACKTRACE_, true);
				if (checkReturn > 0)localErrorCode = checkReturn;
			}
			
			if(districtObject.buildings[square][block].size == 4){
				for(var block in districtObject.buildings[square]){
					if(block == block)continue;
					_STACKTRACE_.addToStack(block);
					var checkReturn = damageBuilding(districtObject, square, block, true, _STACKTRACE_);
					if (checkReturn > 0)localErrorCode = checkReturn;
					_STACKTRACE_.removeFromStack(block);
				}
			}
		}
	}
	
	if (localErrorCode == -1) localErrorCode = 0
	printError(localErrorCode, _STACKTRACE_, [districtObject, square, block], false);
	_STACKTRACE_.removeFromStack('damageBuilding');
	return localErrorCode;
	}

function repairBuilding(districtObject, square, block, _STACKTRACE_, fromLink) {
	fromLink = fromLink || false;
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('repairBuilding');
		else _STACKTRACE_ = new stackTrace('repairBuilding');
	} 
	else _STACKTRACE_ = new stackTrace('repairBuilding');
	var localErrorCode = -1;
	
	breakout:{
		//Check the object path to make sure nothing is undefined
		if (districtObject === undefined || districtObject.__proto__.constructor != cityDistrict) {localErrorCode = 103; break breakout;}
		if (districtObject.buildings[square] === undefined || districtObject.buildings[square].__proto__.constructor != Object) {localErrorCode = 103; break breakout;}
		if (districtObject.buildings[square][block] === undefined || districtObject.buildings[square][block].__proto__.constructor != Object) {localErrorCode = 103; break breakout;}
		
		var cost = districtObject.buildings[square][block].cost / 2;
		var paid;
		if(fromLink === true)paid = true;
		if(fromLink === false){
			if(kingdom.main.buildPoints > cost){
				localErrorCode = kingdom.main.update('buildPoints', kingdom.main.buildPoints - cost, _STACKTRACE_);
				if (localErrorCode === 0)paid = true;
			}
			else {
				localErrorCode = 53;
				paid = false;
			}
		}
		if(paid === true){
			localErrorCode = districtObject.updateBuilding(square, block, 'damaged', false, _STACKTRACE_);
			
			if(fromLink === false){
				if(districtObject.buildings[square][block].size == 2){
					var checkLink;
					if(fromLink === true )checkLink = false;
					else checkLink = true;
					var checkReturn = repairBuilding(districtObject, square, districtObject.buildings[square][block].link, _STACKTRACE_, true);
					if (checkReturn > 0)localErrorCode = checkReturn;
				}
				if(districtObject.buildings[square][block].size == 4){
					for(var block in districtObject.buildings[square]){
						if(block == block)continue;
						_STACKTRACE_.addToStack(block);
						localErrorCode = repairBuilding(districtObject, square, block, _STACKTRACE_, true);
						_STACKTRACE_.removeFromStack(block);
					}
				}
			}
		}
	}
	
	if (localErrorCode == -1) localErrorCode = 0
	printError(localErrorCode, _STACKTRACE_, [districtObject, square, block], false);
	_STACKTRACE_.removeFromStack('repairBuilding');
	return localErrorCode;
	}

function updateRoles(role, name, mod, stats, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('updateRoles');
		else _STACKTRACE_ = new stackTrace('updateRoles');
	}
	else _STACKTRACE_ = new stackTrace('updateRoles');
	var localErrorCode = -1;
	
	var data = {'name':undefined, 'E':0, 'L':0, 'S':0, 'con':0};

	var list = kingdom.main.roleList();

	breakout:{
		if(typeof name != 'string' || name === ''){localErrorCode = -10; break breakout;}
		if(typeof mod != 'number' || isNaN(mod)){localErrorCode = -10; break breakout;}
		if(list.indexOf(role) == -1){localErrorCode = -10; break breakout;}
		if(stats == 'all')stats = ['E', 'L', 'S'];
		else if(stats.constructor == Array) stats = stats;
		else stats = [stats];
		if(typeof stats != 'object'){localErrorCode = -10; break breakout;}
		for(var ELS in stats){
			_STACKTRACE_.addToStack(ELS);
			if(stats[ELS] != 'E' && stats[ELS] != 'L' && stats[ELS] != 'S'){localErrorCode = -10; printError(localErrorCode, _STACKTRACE_, [stats[ELS], ELS], false);_STACKTRACE_.removeFromStack(ELS); break breakout;}
			_STACKTRACE_.removeFromStack(ELS);
		}

		data.name = name;
		for(var ELS in stats){
			_STACKTRACE_.addToStack(ELS);
			data[stats[ELS]] = mod;
			_STACKTRACE_.removeFromStack(ELS);
		}

		kingdom.main.update(role, data, _STACKTRACE_);
		updateKingdom(_STACKTRACE_);
	}
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [role, name, mod, stats], false);
	_STACKTRACE_.removeFromStack('updateRoles');
	return localErrorCode;
}

function updateEdicts(edict, level, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('updateEdicts');
		else _STACKTRACE_ = new stackTrace('updateEdicts');
	}
	else _STACKTRACE_ = new stackTrace('updateEdicts');
	var localErrorCode = -1;
	
	var data = {'name':undefined, 'E':0, 'L':0, 'S':0, 'con':0};

	breakout:{
		if(typeof edict != 'string'){localErrorCode = -10; break breakout;}
		if(typeof level != 'number'){localErrorCode = -10; break breakout;}
		if(edict != 'promotion' && edict != 'taxation' && edict != 'festivals'){localErrorCode = -10; break breakout;}
		if(level < 0 || level > 4){localErrorCode = -10; break breakout;}
		
		switch(level){
			case 0:
				switch(edict){
					case 'promotion':data = {'name':'None', 'E':0, 'L':0, 'S':-1, 'con':0}; break;
					case 'taxation':data = {'name':'None', 'E':0, 'L':1, 'S':0, 'con':0}; break;
					case 'festivals':data = {'name':'None', 'E':0, 'L':-1, 'S':0, 'con':0}; break;
				}
				break;
			case 1:
				switch(edict){
					case 'promotion':data = {'name':'Token', 'E':0, 'L':0, 'S':1, 'con':1}; break;
					case 'taxation':data = {'name':'Light', 'E':1, 'L':-1, 'S':0, 'con':0}; break;
					case 'festivals':data = {'name':'1', 'E':0, 'L':1, 'S':0, 'con':1}; break;
				}
				break;
			case 2:
				switch(edict){
					case 'promotion':data = {'name':'Standard', 'E':0, 'L':0, 'S':2, 'con':2}; break;
					case 'taxation':data = {'name':'Normal', 'E':2, 'L':-2, 'S':0, 'con':0}; break;
					case 'festivals':data = {'name':'6', 'E':0, 'L':2, 'S':0, 'con':2}; break;
				}
				break;
			case 3:
				switch(edict){
					case 'promotion':data = {'name':'Aggresive', 'E':0, 'L':0, 'S':3, 'con':4}; break;
					case 'taxation':data = {'name':'Heavy', 'E':3, 'L':-4, 'S':0, 'con':0}; break;
					case 'festivals':data = {'name':'12', 'E':0, 'L':3, 'S':0, 'con':4}; break;
				}
				break;
			case 4:
				switch(edict){
					case 'promotion':data = {'name':'Expansionist', 'E':0, 'L':0, 'S':4, 'con':8}; break;
					case 'taxation':data = {'name':'Overwhelming', 'E':4, 'L':-8, 'S':0, 'con':0}; break;
					case 'festivals':data = {'name':'24', 'E':0, 'L':4, 'S':0, 'con':8}; break;
				}
				break;
		}
		
		kingdom.main.update(edict, data, _STACKTRACE_);
		updateKingdom(_STACKTRACE_);
	}
	
	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [edict, level], false);
	_STACKTRACE_.removeFromStack('updateEdicts');
	return localErrorCode;
}