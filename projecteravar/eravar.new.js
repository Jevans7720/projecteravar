/*
Project lead: Jeff evans
Last edit by Jeff Evans
date: 07/26/2016
*/

function buildNewDistrict(city, terrain, _STACKTRACE_) {
	if (typeof _STACKTRACE_ == 'object') {
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('buildNewDistrict');
		else _STACKTRACE_ = new stackTrace('buildNewDistrict');
	} else _STACKTRACE_ = new stackTrace('buildNewDistrict');

	var localErrorCode = -1;
	var cost;
	var time;
	var paid;

	breakout: {
		//Determine cost and build time based on terrain type 
		switch(terrain){
			case 'forest': cost = 4; time = 2; break;
			case 'grassland': cost = 1; time = 0; break;
			case 'hills': cost = 2; time = 1; break;
			case 'mountain': cost = 12; time = 4; break;
			case 'swamp': cost = 8; time = 3; break;
			default:
				{localErrorCode = -10; break breakout;}
		}
		if(typeof city != 'string'){localErrorCode = -10; break breakout;}
		if(kingdom[city] === undefined){localErrorCode = -10; break breakout;}
		//Calculate and deduce cost
		if(kingdom.main.buildPoints > cost){
			localErrorCode = kingdom.main.update('buildPoints', kingdom.main.buildPoints - cost, _STACKTRACE_);
			if (localErrorCode === 0)paid = true;
		}
		else {
			localErrorCode = 53;
			paid = false;
		}
		//If the cost was paid build a new district
		if(paid === true){
			var newDistrict = new cityDistrict('new', city, _STACKTRACE_);
			kingdom[city].districts[newDistrict.districtID] = newDistrict;
			newDistrict = undefined;
		}
		updateKingdom(_STACKTRACE_);
	}

	if (localErrorCode == -1) localErrorCode = 0
	printError(localErrorCode, _STACKTRACE_, [], false);
	_STACKTRACE_.removeFromStack('buildNewDistrict');
	return localErrorCode;
}

function buildNewBuilding(districtObj, square, block, building, orientation, _STACKTRACE_){
	if(typeof _STACKTRACE_ == 'object'){
		if (_STACKTRACE_.__proto__.constructor == stackTrace) _STACKTRACE_.addToStack('buildNewBuilding');
		else _STACKTRACE_ = new stackTrace('buildNewBuilding');
	}
	else _STACKTRACE_ = new stackTrace('buildNewBuilding');
	var localErrorCode = -1;
	var cost;
	var link;

	console.log('Halving building costs is currently not supported.');

	breakout:{
		//Check all parameters for undefined/incorrect values and check the block availabliy
		if(buildings[building].size == 2){
			if(orientation === undefined || (orientation != 'wide' && orientation != 'tall')){console.log(orientation);localErrorCode = -10; break breakout;}
		}
		else{
			if(orientation === undefined || orientation != 'N/A'){console.log(orientation);localErrorCode = -10; break breakout;}
		}
		if(districtObj === undefined){console.log(districtObj);localErrorCode = 103; break breakout;}
		if(square === undefined){console.log(square);localErrorCode = -10; break breakout;}
		if(block === undefined){console.log(block);localErrorCode = -10; break breakout;}
		if(building === undefined){console.log(building);localErrorCode = -10; break breakout;}
		if(districtObj.buildings[square][block] != 'Empty'){localErrorCode = 50; break breakout;}
		if(buildings[building] === undefined){localErrorCode = 104; break breakout;}

		//get building cost
		cost = buildings[building].cost;

		//Compare cost to vavailable build points
		if(kingdom.main.buildPoints < cost){localErrorCode = 53; break breakout;}

		//If everything succeeds and the building is small build it.
		if(buildings[building].size == 1){
			localErrorCode = districtObj.newBuilding(square, block, building, _STACKTRACE_);
		}

		//If  the building is size medium figure out what the second block thast will be occupied is
		if(buildings[building].size == 2){
			switch(block){
				case 'blockTR':
					if(orientation == 'wide') link = 'blockTL';
					if(orientation == 'tall') link = 'blockBR';
					break
				case 'blockTL':
					if(orientation == 'wide') link = 'blockTR';
					if(orientation == 'tall') link = 'blockBL';
					break
				case 'blockBR':
					if(orientation == 'wide') link = 'blockBL';
					if(orientation == 'tall') link = 'blockTR';
					break
				case 'blockBL':
					if(orientation == 'wide') link = 'blockBR';
					if(orientation == 'tall') link = 'blockTL';
					break
					break;
				default:
					console.log(block);
					localErrorCode = -10;
					break breakout;
			}

			//If the second block is not free throw an error
			if(districtObj.buildings[square][link] != 'Empty'){localErrorCode = 50; break breakout;}

			//Otherwise add the building to both blocks and link them to each other
			localErrorCode = districtObj.newBuilding(square, block, building, _STACKTRACE_);
			localErrorCode = districtObj.updateBuilding(square, block, 'link', link, _STACKTRACE_);
			localErrorCode = districtObj.newBuilding(square, link, building, _STACKTRACE_);
			localErrorCode = districtObj.updateBuilding(square, link, 'link', block, _STACKTRACE_);
		}

		//For large buildings loop through all blocks
		if(buildings[building].size == 4){
			for(link in districtObj.buildings[square]){

				//Skip over the current block
				if(link == block)continue;
				_STACKTRACE_.addToStack(link);

				//If any block is not empty cancel the build
				if(districtObj.buildings[square][link] != 'Empty'){localErrorCode = 50; _STACKTRACE_.removeFromStack(link); break breakout;}
				_STACKTRACE_.removeFromStack(link);
			}
			for(link in districtObj.buildings[square]){

				//Add the buildings to each block
				localErrorCode = districtObj.newBuilding(square, link, building, _STACKTRACE_);
				localErrorCode = districtObj.updateBuilding(square, link, 'link', block, _STACKTRACE_);
			}
		}

		//Subtract build points after buildings are built incase of errors
		localErrorCode = kingdom.main.update('buildPoints', kingdom.main.buildPoints - cost, _STACKTRACE_);
		updateKingdom(_STACKTRACE_);
		refreshDistrictWindow(_STACKTRACE_);
	}

	if(localErrorCode == -1)localErrorCode = 0 
	printError(localErrorCode, _STACKTRACE_, [districtObj, square, block, building, orientation], false);
	_STACKTRACE_.removeFromStack('buildNewBuilding');
	return localErrorCode;
}